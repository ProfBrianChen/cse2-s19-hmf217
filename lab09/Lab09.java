//Hannah Fabian
//SCE2-S19
//Professor Chen
//12 April 2019
//
//This program will use searching to find specific integers in an array.

import java.util.Random; //imports the existing Random class
import java.util.Arrays; //imports the existing Arrays class
import java.util.Scanner; //imports the existing Scanner class

public class Lab09{ //public class we are in
   
   //method for creating an array of random integers
   public static int[] arrayGenerator(){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Enter the array size: "); //prompts the user for the array size
      int mySize=myScanner.nextInt(); //assigns the input as an integer to mySize
      int[] myArray=new int[mySize]; //declares and allocates myArray with size mySize
      for(int i=0; i<mySize; i++){ //for all entries in the array between 0 and mySize
         myArray[i]=randomGenerator.nextInt(mySize); //assign a random integer to each entry
         System.out.print(myArray[i]+" "); //print the array
      } //end of for loop
      return myArray;
   } //end of arrayGenerator method
   
   //method for creating an ascending array
   public static int[] ascendingArray(){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Enter the array size: "); //prompts the user for the array size
      int mySize=myScanner.nextInt(); //assigns the input as an integer to mySize
      int[] ascendingArray=new int[mySize]; //declares and allocates myArray with size mySize
      for(int i=0; i<mySize; i++){ //for all entries in the array between 0 and mySize
         ascendingArray[i]=randomGenerator.nextInt(mySize); //assign a random integer to each entry
      } //end of for loop
      Arrays.sort(ascendingArray);
      for (int i=0; i<mySize; i++){
         System.out.print(ascendingArray[i]+" "); //print the array
      }
      return ascendingArray;
   } //end of ascending array method
   
   //method for performing a linear search
   public static int linearSearch(int[] myArray, int myTerm){
      for(int i=0;i<myArray.length;i++){    
         if(myArray[i]==myTerm){    
            return i;    
         } //end of if statement
      } //end of for loop
      return -1; //returns -1 if myTerm is never found
   }
   
   //method for permorming binary search
   public static int binarySearch(int[] ascendingArray, int myTerm){
      int i=0;
      int min=0;
      int max=ascendingArray.length-1;
      while (min<=max){
         int mid=(max+min)/2;
         if (ascendingArray[mid]<myTerm){
            min=mid+1;
         }
         else if (ascendingArray[mid]>myTerm){
            max=mid-1;
         }
         else if (ascendingArray[mid]==myTerm){
            i=mid;
            return i;
         }
      }
      return -1;
   }
   
   //main method
   public static void main(String[] args){
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Would you like to do a linear or binary search? "); //prompts user to input type of search
      String linear="linear"; //declares and initializes linear
      String binary="binary"; //declares and initializes binary
      String mySearch=myScanner.next(); //accept the user input data as a String
      if (mySearch.equals(linear)){ //if the input is valid
         int[] myArray=arrayGenerator();
         System.out.println();
         System.out.print("What number are you looking for? ");
         int myTerm=myScanner.nextInt(); //accept the user input data as a String
         int i=linearSearch(myArray, myTerm);
         if (i>=0){
            System.out.println("The integer was found at index "+i+".");
         }
         else{
            System.out.println("The integer cannot be found (i=-1).");
         }
      }
      else if (mySearch.equals(binary)){
         int[] ascendingArray=ascendingArray();
         System.out.println();
         System.out.print("What number are you looking for? ");
         int myTerm=myScanner.nextInt(); //accept the user input data as a String
         int i=binarySearch(ascendingArray, myTerm);
         if (i>=0){
            System.out.println("The integer was found at index "+i+".");
         }
         else{
            System.out.println("The integer cannot be found (i=-1).");
         }
      }
   } //end of main method
   
} //end of public class