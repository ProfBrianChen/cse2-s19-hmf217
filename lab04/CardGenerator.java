//Hannah Fabian
//15 February 2019
//CSE2-S19

//The card generator will pick a random card from a deck of cards.
//Cards 1-13 represent diamonds, 14-26 represent clubs, 27-39 represent hearts,
//and 40-52 represent spades. The cards are represented in ascending order
//(i.e. 1 is Ace of diamonds, 2 is 2 of diamonds, etc.)
//
public class CardGenerator {                        //states the public class we are in
  public static void main(String[] args){           //main method required for every Java program
    //input statements
    int randomNumber=(int)(Math.random()*52)+1;     //declares and assigns the random number, explicitly casted as an integer
    String cardSuit="";                             //declares the name of the suit as a String
    String cardIdentity="";                         //declares the identity of the card as a String
    
    //if statements
    if (randomNumber>=1 && randomNumber<=13){       //if statement with boolean condition
      cardSuit="diamonds";                          //assigns the card suit as diamond
    }                                               //end of if statement
    if (randomNumber>=14 && randomNumber<=26){      //if statement with boolean condition
      cardSuit="clubs";                             //assigns the card suit as club
    }                                               //end of if statement
    if (randomNumber>=27 && randomNumber<=39){      //if statement with boolean condition
      cardSuit="hearts";                            //assigns the card suit as heart
    }                                               //end of if statement
    if (randomNumber>=40 && randomNumber<=52){      //if statement with boolean condition
      cardSuit="spades";                            //assigns the card suit as spade
    }                                               //end of if statement
    
    //switch statement
    switch (randomNumber){                          //switch statement
      case 1: case 14: case 27: case 40:            //states the case values
        cardIdentity="Ace";                         //assigns the Ace to corresponding cases
        break;                                      //stops case fallthrough
      case 2: case 15: case 28: case 41:            //states the case values
        cardIdentity="2";                           //assigns the 2 to corresponding cases
        break;                                      //stops case fallthrough
      case 3: case 16: case 29: case 42:            //states the case values
        cardIdentity="3";                           //assigns the 3 to corresponding cases
        break;                                      //stops case fallthrough
      case 4: case 17: case 30: case 43:            //states the case values
        cardIdentity="4";                           //assigns the 4 to corresponding cases
        break;                                      //stops case fallthrough
      case 5: case 18: case 31: case 44:            //states the case values
        cardIdentity="5";                           //assigns the 5 to corresponding cases
        break;                                      //stops case fallthrough
      case 6: case 19: case 32: case 45:            //states the case values
        cardIdentity="6";                           //assigns the 6 to corresponding cases
        break;                                      //stops case fallthrough
      case 7: case 20: case 33: case 46:            //states the case values
        cardIdentity="7";                           //assigns the 7 to corresponding cases
        break;                                      //stops case fallthrough
      case 8: case 21: case 34: case 47:            //states the case values
        cardIdentity="8";                           //assigns the 8 to corresponding cases
        break;                                      //stops case fallthrough
      case 9: case 22: case 35: case 48:            //states the case values
        cardIdentity="9";                           //assigns the 9 to corresponding cases
        break;                                      //stops case fallthrough
      case 10: case 23: case 36: case 49:           //states the case values
        cardIdentity="10";                          //assigns the 10 to corresponding cases
        break;                                      //stops case fallthrough
      case 11: case 24: case 37: case 50:           //states the case values
        cardIdentity="Jack";                        //assigns the Jack to corresponding cases
        break;                                      //stops case fallthrough
      case 12: case 25: case 38: case 51:           //states the case values
        cardIdentity="Queen";                       //assigns the Queen to corresponding cases
        break;                                      //stops case fallthrough
      case 13: case 26: case 39: case 52:           //states the case values
        cardIdentity="King";                        //assigns the King to corresponding cases
        break;                                      //stops case fallthrough
    }                                               //end of switch statement
    
    //print statement
    System.out.println("You picked the " + cardIdentity + " of " + cardSuit); //prints the generated card
    
  }                                                 //end of main method
}                                                   //end of class