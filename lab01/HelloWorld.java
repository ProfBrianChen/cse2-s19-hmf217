///////////
//// CSE 02 Hello World
///
public class HelloWorld{
  
  public static void main(String args[]){
int myFactor=2; 
int inputValue=35; //sentinel variable 
while (myFactor<inputValue){ //boolean condition saying when to run while loop
  if (inputValue % myFactor == 0){ //if this is true
      System.out.println("Factor: " + myFactor); //do this
	break; //stops the if loop from running ever again
     } //end of if statement
  myFactor++; //incrementing the counter
} //end of while loop
  }
}