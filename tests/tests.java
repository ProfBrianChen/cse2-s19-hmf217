import java.util.ArrayList;
import java.util.Scanner;

//----------------------------------------------------------------------
// CardDeck.java           by Dale/Joyce/Weems                 Chapter 6
//
// Models a deck of cards. Includes shuffling and dealing.
//----------------------------------------------------------------------

import java.util.Random;
import java.util.ArrayList;
import java.util.Iterator;

public class CardDeck {
  public static final int NUMCARDS = 52;
  
  protected ArrayList<Card> deck;
  protected Iterator<Card> deal;
  
  public CardDeck() {
    deck = new ArrayList<Card>(NUMCARDS);
    for (Card.Suit suit : Card.Suit.values())
       for (Card.Rank rank : Card.Rank.values()) {     
         deck.add(new Card(rank, suit));
       }
    deal = deck.iterator();
  }

  /**
   * Randomizes the order of the cards in the deck.
   * Resets the current deal.
   */
  public void shuffle() {
    Random rand = new Random(); // to generate random numbers 
    int randLoc;                // random location in card deck
    Card temp;                  // for swap of cards
    
    for (int i = (NUMCARDS - 1); i > 0; i--) {
      randLoc = rand.nextInt(i);  // random integer between 0 and i - 1
      temp = deck.get(randLoc);
      deck.set(randLoc, deck.get(i));
      deck.set(i, temp);
    }
    
    deal = deck.iterator();
  }
  
  /**
   * Returns true if there are still cards left to be dealt; 
   * otherwise, returns false.
   * @return
   */
  public boolean hasNextCard() {
    return (deal.hasNext());
  }
  
  /**
   * Precondition:  this.hasNextCard() == true
   * Returns the next card for the current 'deal'.
   * @return the next Card
   */
  public Card nextCard() {
    return deal.next();
  }
}
 
/**
 * Supports playing card objects having a suit and a rank.
 * Only rank is used when comparing cards. Ace is  "high".
 * @author Dale/Joyce/Weems modified by Henry Carter
 *
 */
public class Card implements Comparable<Card> {

	//Rank is an enum with a value associated with it
	//You will need the value to calculate the hand of the dealer and player.
	//To get the value, use card.rank.getValue()
	  public enum Rank {
		  Two(2), 
		  Three(3), 
		  Four(4), 
		  Five(5), 
		  Six(6), 
		  Seven(7), 
		  Eight(8), 
		  Nine(9), 
	      Ten(10), 
	      Jack(10), 
	      Queen(10), 
	      King(10), 	      
	      Ace(11);
	  
	      private int value;
		  private Rank(int value){
			  this.value = value;
		  }
		  				
		  public int getValue(){
		  		return value;
		  }
	  }

	public enum Suit {Club, Diamond, Heart, Spade}
	
	protected final Rank rank;
	protected final Suit suit;
    
	Card(Rank rank, Suit suit)  {
		this.rank = rank; 
		this.suit = suit; 
	}

	public Rank getRank() { 
	  return rank; 
	}
	
	public Suit getSuit() { 
	  return suit; 
	}

    @Override 
    /**
     * Returns true if 'obj' is a Card with same rank  
     * as this Card, otherwise returns false.
     */
  	public boolean equals(Object obj) {
    	if (obj == this)
            return true;
    
        if (obj == null || obj.getClass() != this.getClass())
             return false;
         else {
             Card c = (Card) obj; 
             return (this.rank == c.rank);
         }
    }

 
    /**
     * Compares this Card with 'other' for order. Returns a 
     * negative integer, zero, or a positive integer as this object 
     * is less than, equal to, or greater than 'other'.
     */
    public int compareTo(Card other) {
    	return this.rank.compareTo(other.rank);
    }

  @Override
  public String toString() { 
	  return suit + " " + rank; 
  }
}
 

public class CardTester {
	 
	public static void main(String[] args) {
		
		//create a new deck of cards
		Scanner scan = new Scanner(System.in);
		int handSize =5;
		int sum =0; 
		int dSum =0;
		char reply;
		
		Card card, card1,card2, dealer, dealer1, dealer2;
		CardDeck deck = new CardDeck();
		CardDeck playDeck = new CardDeck();
		playDeck.shuffle();
		
		ArrayList<Card> hand = new ArrayList<Card>(handSize);
		ArrayList<Card> dHand = new ArrayList<Card>(handSize);

		
		//traverse deck showing card's suit, rank and value
		while(deck.hasNextCard()) {
			Card c = deck.nextCard();
			
			int cardValue = c.rank.getValue();
			System.out.println(c.toString()+" - has suit "+c.getSuit() 
			+ " and rank "+c.getRank()+" and value  "+cardValue);
		}
	
		System.out.println();
		System.out.println("Welcome to BlackJack");
		card = playDeck.nextCard();	
		card1 = playDeck.nextCard();
		dealer = playDeck.nextCard();
		dealer1 =playDeck.nextCard();
		hand.add(card);
		hand.add(card1);
		dHand.add(dealer);
		dHand.add(dealer1);
		
		System.out.println("Your hand: ");
		for (Card c: hand) {
			System.out.print(c+ ", ");
			sum += c.rank.getValue();
		}
		System.out.println(" Your Sum: "+ sum);
		
		System.out.println("\nDealers hand: ");
		System.out.print(dealer);
		for (Card c: dHand) {
			dSum += c.rank.getValue();
		}
		
		System.out.println("\nDraw? Yes(Y)/No(N)");
		reply = scan.nextLine().charAt(0);
		
		if(reply == 'N') {
			if(dSum < 17) {
				dealer2= playDeck.nextCard();
				dHand.add(dealer2);
				dSum += dealer2.rank.getValue();
			} 
		if(reply == 'Y') {
			card2= playDeck.nextCard();
			hand.add(card2);
			sum += card2.rank.getValue();
		}
			
		if(dSum<21 && sum <21) {
			if (dSum > sum) {
				System.out.println("The dealers sum: "+ dSum + "\nYour sum: "+ sum);
				System.out.println("You lost");
			} else
			if (dSum < sum ) {
				System.out.println("The dealers sum: "+ dSum + "\nYour sum: "+ sum);
				System.out.println("You Won");
			}
		}
			
		if(dSum >21) {
			System.out.println("You Win");
		} else
		if(sum >21) {
			System.out.println("You Lost");
		}
		
		}
	}

}
