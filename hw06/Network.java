//Hannah Fabian
//18 March 2019
//CSE2-S19
//
//This program will use loops to print a network of boxes.
//
import java.util.Scanner;                         //imports the existing Scanner class
public class Network{                             //states the public class we are in
  public static void main(String[] args){         //main method required for every Java program
    Scanner myScanner=new Scanner(System.in);     //declares and constructs a Scanner that is ready to accept input
    
    //height input statements
    System.out.print("Enter the height: ");       //prompts the user to input an integer between 1 and 10
    int myHeight=1;                               //declares and initializes the input as an integer
    boolean heightIsAnInteger=myScanner.hasNextInt(); //determines if the input is an integer
    
    //determining if the height input is valid
    if (heightIsAnInteger==true){                 //if the input is an integer
      myHeight=myScanner.nextInt();               //accept the user input data as an integer
      while (myHeight<=0){                        //while the integer is out of range
        System.out.println("ERROR: Input out of range."); //displays error message
        System.out.print("Enter the height: ");   //prompts the user to input an integer
        heightIsAnInteger=myScanner.hasNextInt(); //determines if input is an integer
        if (heightIsAnInteger==true){             //if the input is an integer
          myHeight=myScanner.nextInt();           //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (heightIsAnInteger==false){           //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("ERROR: Invalid input."); //displays error message
        System.out.print("Enter the height: "); //prompts the user to input an integer
        heightIsAnInteger=myScanner.hasNextInt(); //determines if input is an integer
        if (heightIsAnInteger==true){             //if the input is an integer
          myHeight=myScanner.nextInt();           //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //width input statements
    System.out.print("Enter the width: ");       //prompts the user to input an integer between 1 and 10
    int myWidth=1;                               //declares and initializes the input as an integer
    boolean widthIsAnInteger=myScanner.hasNextInt();   //determines if the input is an integer
    
    //determining if the width input is valid
    if (widthIsAnInteger==true){                  //if the input is an integer
      myWidth=myScanner.nextInt();                //accept the user input data as an integer
      while (myWidth<=0){                         //while the integer is out of range
        System.out.println("ERROR: Invalid input."); //displays error message
        System.out.print("Enter the width: ");    //prompts the user to input an integer
        widthIsAnInteger=myScanner.hasNextInt();  //determines if input is an integer
        if (widthIsAnInteger==true){              //if the input is an integer
          myWidth=myScanner.nextInt();            //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (widthIsAnInteger==false){            //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("ERROR: Invalid input."); //displays error message
        System.out.print("Enter the width: ");  //prompts the user to input an integer
        widthIsAnInteger=myScanner.hasNextInt();  //determines if input is an integer
        if (widthIsAnInteger==true){              //if the input is an integer
          myWidth=myScanner.nextInt();            //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //square size input statements
    System.out.print("Enter the square size: ");  //prompts the user to input an integer between 1 and 10
    int mySquareSize=1;                           //declares and initializes the input as an integer
    boolean squareSizeIsAnInteger=myScanner.hasNextInt(); //determines if the input is an integer
    
    //determining if the square size input is valid
    if (squareSizeIsAnInteger==true){             //if the input is an integer
      mySquareSize=myScanner.nextInt();           //accept the user input data as an integer
      while (mySquareSize<=0){                    //while the integer is out of range
        System.out.println("ERROR: Invalid input."); //displays error message
        System.out.print("Enter the square size: "); //prompts the user to input an integer
        squareSizeIsAnInteger=myScanner.hasNextInt(); //determines if input is an integer
        if (squareSizeIsAnInteger==true){         //if the input is an integer
          mySquareSize=myScanner.nextInt();       //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (squareSizeIsAnInteger==false){       //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("ERROR: Invalid input."); //displays error message
        System.out.print("Enter the square size: "); //prompts the user to input an integer
        squareSizeIsAnInteger=myScanner.hasNextInt(); //determines if input is an integer
        if (squareSizeIsAnInteger==true){         //if the input is an integer
          mySquareSize=myScanner.nextInt();       //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement

    //edge length input statements
    System.out.print("Enter the edge length: ");  //prompts the user to input an integer between 1 and 10
    int myEdgeLength=1;                           //declares and initializes the input as an integer
    boolean edgeLengthIsAnInteger=myScanner.hasNextInt(); //determines if the input is an integer
    
    //determining if the edge length input is valid
    if (edgeLengthIsAnInteger==true){             //if the input is an integer
      myEdgeLength=myScanner.nextInt();           //accept the user input data as an integer
      while (myEdgeLength<=0){                    //while the integer is out of range
        System.out.println("ERROR: Invalid input."); //displays error message
        System.out.print("Enter the edge length: "); //prompts the user to input an integer
        edgeLengthIsAnInteger=myScanner.hasNextInt(); //determines if input is an integer
        if (edgeLengthIsAnInteger==true){         //if the input is an integer
          myEdgeLength=myScanner.nextInt();       //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (edgeLengthIsAnInteger==false){       //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("ERROR: Invalid input."); //displays error message
        System.out.print("Enter the edge length: "); //prompts the user to input an integer
        edgeLengthIsAnInteger=myScanner.hasNextInt(); //determines if input is an integer
        if (edgeLengthIsAnInteger==true){         //if the input is an integer
          myEdgeLength=myScanner.nextInt();       //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    
    //printing the network
    int loop= mySquareSize+myEdgeLength;          //the sum of the the square size and the edge length
    for (int k=0; k<myHeight; k++){               //determines the height of the network
      for (int m=0; m<myWidth; m++){              //determines the width of the network
       if((m % loop==0 || m%loop==(mySquareSize-1))){ //conditions for either the # or the | to print
         if( (k % loop==0 || k%loop==(mySquareSize-1))){ //conditions for the # to print
           System.out.print("#");                 //prints the # in the corner
         }                                        //end of if statement
         else{                                    //any other conditions
          System.out.print("|");                  //print |
         }                                        //end of else statement
        }                                         //end of if statement
        else if (m%loop==((mySquareSize/2)-1) || m%loop==((mySquareSize/2))){ //conditions for either the - or the | to print
          if (((k%loop)==0) && ((k%loop)==mySquareSize-1)){ //conditions for the - to print
            System.out.print("-");                //print -
          }                                       //end of if statement
          System.out.print("|");                  //print |
        }                                         //end of else if statement
        else if((m%loop<(mySquareSize-1)) && (k%loop<(mySquareSize)) ){ //conditions for the - to print
          if (((k%loop)!=0) && ((k%loop)!=mySquareSize-1)){ //conditions to break the loop
            break;                                //break the loop
          }                                       //end of if statement
          System.out.print("-");                  //print -
        }                                         //end of else if statement
        else if (k%loop==((mySquareSize/2)-1) || k%loop==((mySquareSize/2))){ //conditions for either the - or | to print
          if (((m%loop)==0) && ((m%loop)==mySquareSize-1)){ //conditions for the | to print
            System.out.print("|");                //print |
          }                                       //end of if statement
          System.out.print("-");                  //print -
        }                                         //end of else if statement
        else {                                    //any ither conditions
           System.out.print(" ");                 //print a blank space
        }                                         //end of else statement
      }                                           //end of first for loop (horizontal outputs)                      
      System.out.println("");                     //print on a new line
    }                                             //end of second for loop (verticle outputs)
    
    
    /*for (int k=1; k<=myHeight; k++){               //while k is less than or equal the width
      //for (int m=1; m<=myWidth; m++){            //while m is less than or equal to the height
        for (int i=1; i<=mySquareSize; i++){      //while i is less than or equal to the square size
          for (int j=1; j<=mySquareSize; j++){    //while j is less than or equal to the square size
            if (i==1&&j==1 || i==mySquareSize&&j==mySquareSize || i==1&&j==mySquareSize || j==1&&i==mySquareSize){ //corners
              System.out.print("#");              //prints pound sign in corners
            }                                     //end of if statement
            else if (j==1 || i==mySquareSize+1 || j==mySquareSize){ //verticle edges
              System.out.print("|");              //prints | on verticle edges
            }                                     //end of else if statement
            else if (i==1 || i==mySquareSize){    //horizontal edges
              System.out.print("-");              //prints - on horizontal edges
            }                                     //end of else if statement
            else{                                 //any other parts that aren't part of the square
              System.out.print(" ");              //print a space
            }                                     //end of else statement
          }                                       //end of first for loop
          System.out.println("");                 //prints next square on a new line
        }                                         //end of second for loop
      //}                                           //end of third for loop
    //}                                             //end of fourth for loop
    
    
    
    for (int i=1, j=1; j<=myWidth/(mySquareSize+myEdgeLength); j++){ //determines width of network
      for (; i<=myHeight/(mySquareSize+myEdgeLength); i++){ //determines height of network
        if (i==1&&j==1 || i==mySquareSize&&j==mySquareSize || i==1&&j==mySquareSize || j==1&&i==mySquareSize){ //corners
          System.out.print("#");                    //prints pound sign in corners
        }                                           //end of if statement
        else if (i==1 || i==mySquareSize){          //horizontal edges
          System.out.print("-");                    //prints - on horizontal edges
        }                                           //end of else if statement    
      }                                             //end of for loop
      if (i==1&&j==1 || i==mySquareSize&&j==mySquareSize || i==1&&j==mySquareSize || j==1&&i==mySquareSize){ //corners
        System.out.print("#");                    //prints pound sign in corners
      }                                           //end of if statement
      else if (j==1 || i==mySquareSize+1 || j==mySquareSize){ //verticle edges
        System.out.print("|");              //prints | on verticle edges
      }                                     //end of else if statement                                
    }                                             //end of for loop
    */
    
  }                                               //end of main method
}                                                 //end of public class