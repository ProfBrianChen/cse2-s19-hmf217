//Hannah Fabian
//Professor Chen
//CSE2-S19
//2 May 2019

import java.util.Random; //imports the existing Random class
import java.util.Arrays; //imports the existing Arrays class

//public class we are in
public class Straight{
  
  //method for shuffled deck of cards
  public static int[] deck(){
     int[] deck=new int[52]; //declares and allocates deck
     for (int i=0; i<deck.length; i++){ //for all indexes
        deck[i]=i; //assign a number from 0-51
     } //end of for loop
     for (int i=0; i<deck.length; i++){ //for all indexes
         int target=(int) (deck.length*Math.random()); //randomize the values
         int temp=deck[target];
	      deck[target]=deck[i];
	      deck[i]=temp;
     } //end of for loop
     return deck; //return the deck for use in other methods
  } //end of deck method
  
   //method for selecting five cards
   public static int[] hand(int[] myDeck){
      int[] myHand=new int[5]; //declared and allocates hand
      for (int i=0; i<5; i++){ //for all indexes
         myHand[i]=myDeck[i]; //pick a card
      } //end of for loop
      return myHand; //return myHand for use in other methods
   } //end of hand method
   
    //method for searching for the lowest card
    public static int search(int[] myHand, int k){ 
       for(int i=0;i<myHand.length;i++){ //for all indexes
          if(myHand[i]==k){ //if the value at that index equals k
             return i; //return i for use in other methods
          } //end of if statement
       } //end of for loop
      return -1; //returns -1 if myTerm is never found    
    } //end of search method
   
   //method for determining if it is a straight
   public static boolean straight(int[] myHand){
      Arrays.sort(myHand); //sort the array in ascending order
      for (int i=0; i<myHand.length-1; i++){ // for all indexes
         if (myHand[i+1]==myHand[i]+1){ //if there is a straight
            return true; //return that there is a straigh
         } //end of if statement
      } //end of for loop
      return false; //return false otherwise
   } //end of straight method
   
   //main method
   public static void main(String[] args){
      int counter=0; //declares and initializes the counter
      for (int i=0; i<1000000; i++){ //for 1 million times
         int[] myDeck=deck(); //call deck method
         int[] myHand=hand(myDeck); //call hand method
         boolean isStraight=straight(myHand); //call straight method
         if (isStraight==true){ //if there is a straight
            counter++; //add to the counter
         } //end of if statement
         if (isStraight==false){ //if there is not a straight
            continue; //continue through the for loop
         } //end of if statement
      } //end of for loop
      double myPercentage=((double) counter)/1000000; //calculate the percentage of straights
      System.out.println(myPercentage+"%"); //print the percentage
   } //end of main method
} //end of public class