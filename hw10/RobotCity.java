//Hannah Fabian
//Professor Chen
//CSE2-S19
// 30 April 2019

import java.util.Random; //imports the existing Random class

//public class we are in
public class RobotCity{
   
   //method to build the city
   public static int[][] buildCity(){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      int i=randomGenerator.nextInt(15-10)+10; //randomly generate an integer
      int j=randomGenerator.nextInt(15-10)+10; //randomly generate an integer
      int[][] cityArray=new int[i][j]; //declare a new array
      for (int k=0; k<cityArray.length; k++){ //for all indexes in the array
         for (int m=0; m<cityArray[k].length; m++){ //for all second dimension indexes in the array
            cityArray[k][m]=randomGenerator.nextInt(999-100)+100; //allocate the array
         } //end of for loop
      } //end of for loop
      return cityArray; //return the array
   } //end od buildCity method
   
   //method for printing the city
   public static void display(int[][]cityArray){
      for (int i=0; i<cityArray.length-1; i++){ //for all indexes in the array
         for (int j=0; j<cityArray[i].length-1; j++){ //for all second dimension indexes in the array
            System.out.printf(cityArray[i][j]+"\t"); //print the array
         } //end of for loop
         System.out.printf("%n"); //prints a new line
      } //end of for loop
   } //end of display method
   
   //method for invading robots
   public static int[][] invade(int[][] cityArray, int k){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      for (int o=0; o<k; o++){ //for all values until k
         int i=randomGenerator.nextInt(cityArray.length-1); //pick random indexes
         int j=randomGenerator.nextInt(cityArray[i].length-1); //pick random indexes
         cityArray[i][j]=(-1)*(cityArray[i][j]); //negate those indexes
      } //end of for loop
      return cityArray; //return the invaded cityArray
   }
   
    //methods for updating the city
    public static int[][] update(int[][] cityArray){
       for (int k=cityArray.length-1; k>=0; k--){ //for all indexes in the array
          for (int m=cityArray[0].length-1; m>=0; m--){ //for all second dimension indexes in the array
             if (cityArray[k][m]<0){ //if the array element is negative
                if (m==cityArray[0].length-1){ //if the element is on the end of the map
                   cityArray[k][m]=(-1)*(cityArray[k][m]); //negate it
                } //end of if statement
                else{ //if anything else
                   cityArray[k][m]=(-1)*(cityArray[k][m]); //negate it (thus making it positive again)
                   cityArray[k][m+1]=(-1)*cityArray[k][m+1]; //negate the one next to it
                } //end of else statement
             } //end of if statement
          } //end of for loop
       } //end of for loop
       return cityArray; //return updated arrayCity
    } //end of update method
   
   //main method
   public static void main(String[] args){
      for (int i=0; i<5; i++){ //for five times
         Random randomGenerator=new Random(); //declares and constructs a Random() object
         int[][] cityArray=buildCity(); //declares and alloctaes cityArray
         System.out.println("Building the city:");
         display(cityArray); //print cityArray
         System.out.println();
         System.out.println("Invading the city:");
         int k=randomGenerator.nextInt(50-10)+10; //declare and initialize a random integer
         cityArray=invade(cityArray, k); //allocate invaded cityArray
         display(cityArray); //print invaded cityArray
         cityArray=update(cityArray); //allocate updated cityArray
         System.out.println();
         System.out.println("Updating the city:");
         display(cityArray); //print updated cityArray
         System.out.println();
         System.out.println();
      } //end of for loop
   } //end of main method
} //end of public class