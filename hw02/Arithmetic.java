//Hannah Fabian
//5 February 2019
//CSE2-S19
//
//This program computes the total cost of items purchased at a store,
//with the PA sales tax of 6% applied.
//
public class Arithmetic {                           //states the public class we are in
  public static void main(String[] args){           //main method required for every Java program
        
    //input data
    int numPants=3;                                 //declares and assigns the number of pants bought as an integer
    double pricePants=34.98;                        //declares and assigns the price of 1 pair of pants as a double
    int numShirts=2;                                //declares and assigns the number of shirts bought as an integer
    double priceShirts=24.99;                       //declares and assigns the price of 1 shirt as a double
    int numBelts=1;                                 //declares and assigns the number of belts bought as an integer
    double priceBelts=33.99;                        //declares and assigns the price of 1 belt as a double
    double PASalesTax=0.06;                         //declares and assigns the price of PA sales tax as a double
    
    //intermediate variables and output data
    double totalPants=numPants*pricePants;          //declares and assigns the total price of all the pants as a double
    double totalShirts=numShirts*priceShirts;       //declares and assigns the total price of all the shirts as a double
    double totalBelts=numBelts*priceBelts;          //declares and assigns the total price of all the belts as a double
    double taxPants=totalPants*PASalesTax;          //declares and assigns the sales tax on the pants as a double
    double taxShirts=totalShirts*PASalesTax;        //declares and assigns the sales tax on the shirts as a double
    double taxBelts=totalBelts*PASalesTax;          //declares and assigns the sales tax on the belts as a double
    double preTax=totalPants+totalShirts+totalBelts; //declares and assigns the pretax total as a double
    double totalTax=taxPants+taxShirts+taxBelts;    //declares and assigns the total price of sales tax as a double
    double postTax=preTax+totalTax;                 //declares and assigns the posttax total as a double
    
    //eliminate extra decimal places
    taxPants=taxPants*100;                          //limits the decimal places for the sales tax on the pants to 2
    taxPants=(int)taxPants;                         //
    taxPants=taxPants/100.0;                        //
    taxShirts=taxShirts*100;                        //limits the decimal places for the sales tax on the shirts to 2
    taxShirts=(int)taxShirts;                       //
    taxShirts=taxShirts/100.0;                      //
    taxBelts=taxBelts*100;                          //limits the decimal places for the sales tax on the belt to 2
    taxBelts=(int)taxBelts;                         //
    taxBelts=taxBelts/100.0;                        //
    totalTax=totalTax*100;                          //limits the decimal places for the total sales tax to 2
    totalTax=(int)totalTax;                         //
    totalTax=totalTax/100.0;                        //
    postTax=postTax*100;                            //limits the decimal places for the posttax total to 2
    postTax=(int)postTax;                           //
    postTax=postTax/100.0;                          //
    
    //printing the output data
    System.out.println("The pants cost $"+totalPants+".");                  //prints the pretax cost of the pants
    System.out.println("The shirts cost $"+totalShirts+".");                //prints the pretax cost of the shirts
    System.out.println("The belt cost $"+totalBelts+".");                   //prints the pretax cost of the belt
    System.out.println("The sales tax for the pants is $"+taxPants+".");    //prints the sales tax for the pants
    System.out.println("The sales tax for the shirts is $"+taxShirts+".");  //prints the sales tax for the shirts
    System.out.println("The sales tax for the belt is $"+taxBelts+".");     //prints the sales tax for the belt
    System.out.println("The pretax total is $"+preTax+".");                 //prints the pretax total
    System.out.println("The total sales tax is $"+totalTax+".");      //prints the total sales tax
    System.out.println("The posttax total is $"+postTax+".");               //prints the posttax total
 
  }                                                 //end of main method
}                                                   //end of class