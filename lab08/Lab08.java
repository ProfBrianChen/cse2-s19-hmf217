//Hannah Fabian
//CSE2-S19
//5 April 2019
//
//This program will determine the range, mean, and standard deviation of an array of random size.

import java.util.Arrays; //imports the existing Arrays class
import java.util.Random; //imports the existing Random class
import java.lang.Math; //imports the existing Math class
public class Lab08{ //states public class we are in
   
   //main method
   public static void main(String[] args){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      int mySize=randomGenerator.nextInt((100-50)+1)+50; //assigns a random integer to mySize
      int[] myArray=new int[mySize]; //declares and allocates myArray with size mySize
      System.out.println("The size of the array is "+mySize+'.'); //prints the size of the array
      for(int i=0; i<mySize; i++){ //for all entries in the array between 0 and mySize
         myArray[i]=randomGenerator.nextInt(100); //assign a random integer to each entry
         System.out.print(myArray[i]+" "); //print the array
      }
      System.out.println();
      int range=getRange(myArray); //gets the range from getRange method
      System.out.println("The range of the array is "+range+'.'); //prints the range
      double mean=getMean(myArray, mySize); //gets the mean from getMean method
      System.out.println("The mean of the array is "+mean+'.'); //prints the mean
      double stdDev=getStdDev(myArray, mySize); //gets the standard deviation from getStDev method
      System.out.println("The standard deviation of the array is "+stdDev+'.'); //prints the standard deviation
      shuffle(myArray);
      System.out.println();
   } //end of main method
   
   
     //range method
     public static int getRange(int[] myArray){
        int min=myArray[0]; //assigns the minimum value of the array
        int max=myArray[0]; //assigns the maximum value of the array
        for (int k=1; k<myArray.length; k++){ //for all indexes
           if (myArray[k]<min){ //if the value is less than the min
              min=myArray[k]; //make that the new min
           } //end of if statement
           else if (myArray[k]>max){ //if the value is greater than the max
              max=myArray[k]; //make that the new max
           } //end of else if statement
        } //end of for loop
        int range=max-min; //the range is the max value minus the min value
        return range; //return the range for use in the main method
     } //end of range method
   
   
     //mean method
     public static double getMean(int[] myArray, int mySize){
        double sum=0; //initializes the sum as 0
        for (int m=0; m<myArray.length; m++){ //for all indexes
           sum=sum+myArray[m]; //add the values of the arrays
        } //end of for loop
        double mean=sum/mySize; //the mean is the sum divided by the size of the array
        return mean; //return the mean for use in the main method
     } //end of mean method
   
   
    //standard deviation method
    public static double getStdDev(int[] myArray, int mySize){
       double sum2=0; //initializes the sum as 0
       for (int m=0; m<myArray.length; m++){ //for all indexes
           sum2=sum2+myArray[m]; //add the values of the arrays
       } //end of for loop
       double mean=sum2/mySize; //the mean is the sum divided by the size of the array
       double sum1=0; //initializes the sum as 0
       for (int n=0; n<myArray.length; n++){ //for all indexes
           sum1=sum1+(myArray[n]-mean); //add the values of the arrays
        } //end of for loop
       double myNumerator=sum1*sum1; //squares the summation
       double myDenominator=mySize-1; //sandard deviation denominator
       double stdDev=Math.sqrt(myNumerator/myDenominator); //takes the square root of the numerator/denominator
       return stdDev; //returns the stdDev for future use in the main method
    } //end of standard deviation method
   
   //shuffle method
   public static void shuffle(int[] myArray){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      for (int i=0; i<myArray.length; i++){ //for all indexes in the array
         int swap=randomGenerator.nextInt(myArray.length); //randomly pick an index
         int myShuffledArray=myArray[i]; //the Shuffled array is the original array
         myArray[i]=myArray[swap]; //the original array is the randomized array
         myArray[swap]=myShuffledArray; //the randomized array is the shuffled array
         System.out.print(myArray[i]+" "); //print the array 
      } //end of for loop
   } //end of shuffle method
   
} //end of public class