//Hannah Fabian
//Professor Chen
//CSE2-S19
//9 April 2019
//
//This program will sort letters from an array in two parts: A through M and N through Z.
//
import java.util.Random; //imports existing Random class
public class Letters{ //public class we are in
   
   //main method
   public static void main(String[] args){ 
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      int mySize=randomGenerator.nextInt(25)+1; //assigns a random integer to mySize
      String myAlphabet="QWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm";
      int n=myAlphabet.length();
      String myString="";
      for (int i=1; i<mySize; i++){ //for all indexes in the array
         char myChar=myAlphabet.charAt(randomGenerator.nextInt(n)); //randomly assigns a character
         myString=myString+myChar; //adds the character to the overall String
      } //end of for loop
      System.out.println("Random character array: "+myString); //prints array of random characters
      getAtoM(myString); //runs method finding characters in A to M range
      getNtoZ(myString); //runs methos finding characters in N to Z range
   } //end of main method
   
   //getAtoM method
   public static String getAtoM(String myString){
      String myAtoM=""; //declares and initialized myAtoM
      for (int j=0; j<myString.length(); j++){ //for all characters in the String
         char myChar2=myString.charAt(j); //define the char as the character in that specific place
         if (myChar2<='M' && myChar2>='A'){ //if the char is in the range
            myAtoM=myAtoM+myChar2; //add it to the A to M array
         } //end of if statement
         if (myChar2<='m' && myChar2>='a'){ //if the char is in the range
            myAtoM=myAtoM+myChar2; //add it to the A to M array
         } //end of if statement
      } //end of for loop
      System.out.println("A to M characters: "+myAtoM); //prints the array of characters in the range
      return myAtoM; //returns myAtoM for use in the main method
   } //end of getAtoM method
   
    //getNtoZ method
    public static String getNtoZ(String myString){
      String myNtoZ=""; //declares and initialized myNtoZ
      for (int j=0; j<myString.length(); j++){ //for all characters in the String
         char myChar3=myString.charAt(j); //define the char as the character in that specific place
         if (myChar3<='Z' && myChar3>='N'){ //if the char is in the range
            myNtoZ=myNtoZ+myChar3; //add it to the N to Z array
         } //end of if statement
         if (myChar3<='z' && myChar3>='n'){ //if the char is in the range
            myNtoZ=myNtoZ+myChar3; //add it to the N to Z array
         } //end of if statement
      } //end of for loop
      System.out.println("N to Z characters: "+myNtoZ); //prints the array of characters in the range
      return myNtoZ; //returns myAtoM for use in the main method
    } //end of getNtoZ method
   
} //end of public class