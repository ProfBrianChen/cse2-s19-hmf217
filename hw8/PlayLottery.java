//Hannah Fabian
//Professor Chen
//CSE2-S19
//9 April 2019
//
//This program will use arrays to allow the user to play the lottery!
//
import java.util.Scanner; //imports the existing Scanner class
import java.util.Random; //imports existing Random class
public class PlayLottery{ //name of public class we are in
  
   //main method
   public static void main (String[] args){
      Scanner myScanner=new Scanner(System.in); //constructs the Scanner
      int[] user=new int[5]; //declares an array with a length of 5
      System.out.print("Please enter the 5 lottery numbers that you want (from 0 to 59): "); //prompts user for input
      System.out.println(); 
      for (int i=0; i<5; i++){ //for all indexes of the array
         user[i]=myScanner.nextInt(); //assign each element of the array what they Scanner has accepted
      } //end of for loop 
      int[] winning=numbersPicked(); //declares an array that consists of the five winning numbers
      userWins(user, winning); //determined of the user wins using the userWins method
   } //end of main method
   
   //method picking the winning numbers
   public static int[] numbersPicked(){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      int[] winning=new int[5]; //declares an array with a length of 5
      System.out.print("The winning numbers are: "); //tells the user that the winning numbers are:
      for (int i=0; i<5; i++){ //for each index of the array
         winning[i]=randomGenerator.nextInt(60); //determine a winning number
         System.out.print(winning[i]+" "); //print the list of winning numbers
      } //end of for loop
      return winning; //returns the array for future u7se in the main method
   } //end of numbersPicked method

   //method determining if the user wins
   public static boolean userWins(int[] user, int[] winning){
      boolean didWin=false; //declares and initializes didWin as a boolean
      if (user==winning){ //if the user picked the right numbers
         didWin=true; //didWin is true
         System.out.println(); 
         System.out.println("YOU WON!"); //tells the user they won
      } //end of if statement
      else{ //if the user did not pick the right numbers
         System.out.println(); 
         System.out.println("You lose. :( "); //tells the user that they lost
      } //end of else statement
      return didWin; //returns didWin for future use in the main method
   } //end of userWins method
   
} //end of public class