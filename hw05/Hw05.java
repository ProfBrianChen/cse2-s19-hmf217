//Hannah Fabian
//5 March 2019
//CSE2-S19
//
//This program will use loops to print student schedule information.
//
import java.util.Scanner;                         //imports the existing Scanner class
public class Hw05{                                //states the public class we are in
  public static void main(String[] args){         //main method required for every Java program
    Scanner myScanner=new Scanner(System.in);     //declares and constructs a Scanner that is ready to accept input
    
    //course number information
    System.out.print("Enter your course number: "); //prompts the user to input the course number
    int myCourseNumber=0;                         //declares and initializes course number as an integer
    boolean isCourseNumber=myScanner.hasNextInt(); //determines if the input is an integer
    if (isCourseNumber==true){                    //if the input is an integer
      myCourseNumber=myScanner.nextInt();         //accept the user input data as an integer
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (isCourseNumber==false){              //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("Error: Input an integer."); //tells user they've input data that is not an integer
        System.out.print("Enter your course number: "); //prompts the user to input an integer
        isCourseNumber=myScanner.hasNextInt();    //determines if input is an integer
        if (isCourseNumber==true){                //if the input is an integer
          myCourseNumber=myScanner.nextInt();     //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
        
    //dept name information
    System.out.print("Enter your department name: "); //prompts the user to input department name
    String myDeptName="";                         //declares and initializes departnent name as String
    boolean isDeptName=myScanner.hasNext();       //determines if the input is a String
    if (isDeptName==true){                        //if the input is a String
      myDeptName=myScanner.next();                //accept the user input data as a String
    }                                             //end of if statement
    else{                                         //if the input is not a String
      while (isDeptName==false){                  //while the input is not a String
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("Error: Input a String."); //tells user they've input data that is not a String
        System.out.print("Enter your department name: "); //prompts the user to input a String
        isDeptName=myScanner.hasNext();           //determines if input is an integer
        if (isDeptName==true){                    //if the input is a String
          myDeptName=myScanner.next();            //accept the user input data as a String
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //number of times class meets per week
    System.out.print("Enter the number of times the class meets per week: "); //prompts the user to input the number of weekly meetings
    int myWeeklyMtngs=0;                          //declares and initializes weekly meetings as an integer
    boolean isWeeklyMtngs=myScanner.hasNextInt(); //determines if the input is an integer
    if (isWeeklyMtngs==true){                     //if the input is an integer
      myWeeklyMtngs=myScanner.nextInt();          //accept the user input data as an integer
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (isWeeklyMtngs==false){               //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("Error: Input an integer."); //tells user they've input data that is not an integer
        System.out.print("Enter the number of times the class meets per week: "); //prompts the user to input an integer
        isWeeklyMtngs=myScanner.hasNextInt();     //determines if input is an integer
        if (isWeeklyMtngs==true){                 //if the input is an integer
          myWeeklyMtngs=myScanner.nextInt();      //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //meeting time information
    System.out.print("Enter the meeting time in the format hhmm: "); //prompts the user to input an integer
    int myTime=0;                                 //declares and initializes meeting time as an integer
    boolean isTime=myScanner.hasNextInt();        //determines if the input is an integer
    if (isTime==true){                            //if the number is an integer
      myTime=myScanner.nextInt();                 //accept the user input data as an integer
    }                                             //end of if statement
    else{                                         //if the number is not an integer
      while (isTime==false){                      //while the number is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("Error: Input an integer."); //tells user they've input data that is not an integer
        System.out.print("Enter the meeting time in the format hhmm: "); //prompts the user to input an integer
        isTime=myScanner.hasNextInt();            //determines if input is an integer
        if (isTime==true){                        //if the number is an integer
          myTime=myScanner.nextInt();             //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //instructor name information
    System.out.print("Enter the name of your intructor: "); //prompts the user to input the name of their instructor
    String myInstructor="";                       //declares and initializes instructor name as a String
    boolean isInstructor=myScanner.hasNext();     //determines if the input is a String
    if (isInstructor==true){                      //if the input is a String
      myInstructor=myScanner.next();              //accept the user input data as a String
    }                                             //end of if statement
    else{                                         //if the input is not a String
      while (isInstructor==false){                //while the input is not a String
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("Error: Input a String type."); //tells user they've input data that is not a String
        System.out.print("Enter the name of your instructor: "); //prompts the user to input a String
        isInstructor=myScanner.hasNext();         //determines if input is a String
        if (isInstructor==true){                  //if the number is a String
          myInstructor=myScanner.next();          //accept the user input data as a String
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //class size information
    System.out.print("Enter the number of students in the class: "); //prompts the user to input the number of students in the class
    int myClassSize=0;                            //declares and initializes class size as an integer
    boolean isClassSize=myScanner.hasNextInt();   //determines if the input is an integer
    if (isClassSize==true){                       //if the input is an integer
      myClassSize=myScanner.nextInt();            //accept the user input data as an integer
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (isClassSize==false){                 //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("Error: Input an integer."); //tells user they've input data that is not an integer
        System.out.print("Enter the number of students in the class: "); //prompts the user to input an integer
        isClassSize=myScanner.hasNextInt();       //determines if input is an integer
        if (isClassSize==true){                   //if the input is an integer
          myClassSize=myScanner.nextInt();        //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
  }                                               //end of main method
}                                                 //end of public class