//Hannah Fabian
//1 February 2019
//CSE2-S19
//
//My bicycle cyclometer prints the number of minutes and counts for each trip 
//and prints the distance of each trip and the combined distance of both trips
//in miles.
//
public class Cyclometer {                           //states the public class we are in
  public static void main(String[] args){           //main method required for every Java program
    //input data
    int secsTrip1=480;                              //declares and assigns the seconds trip 1 took as an integer
    int secsTrip2=3220;                             //declares and assigns the seconds trip 2 took as an integer
    int countsTrip1=1561;                           //declares and assigns the number of rotations in trip 1 as an integer
    int countsTrip2=9037;                           //declares and assigns the number of rotations in trip 2 as an integer
    
    //intermediate variables and output data
    double wheelDiameter=27.0;                      //declares and assigns the wheel diameter as a double
    double PI=3.14159;                              //declares and assigns pi as a double
    double feetPerMile=5280;                        //declares and assigns how many feet are in a mile as a double
    double inchesPerFoot=12;                        //declares and assigns how many inches are in a foot as a double
    double secondsPerMinute=60;                     //declares and assigns how many seconds are in a minute as a double
    double distanceTrip1;                           //declares the distance of trip 1 as a double
    double distanceTrip2;                           //declares the distance of trip 2 as a double
    double totalDistance;                           //declares the distance of the total as a double
    
    //printing the output data
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");   
                                                    //prints the time trip 1 took in minutes and the number of counts of the wheel
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
                                                    //prints the time trip 2 took in minutes and the number of counts of the wheel
    
    //run the calculations and store the values
    distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; //gives the distance of trip 1 in miles (the distance of
                                                    //trip 1 equals the number of counts for trip 1 multiplied by the diameter
                                                    //of the wheel times pi, all divided by inches per foot and feet per mile)

    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //gives the distance of trip 2 in miles (the distance of
                                                    //trip 2 equals the number of counts for trip 2 multiplied by the diameter
                                                    //of the wheel times pi, all divided by inches per foot and feet per mile)
    totalDistance=distanceTrip1+distanceTrip2;      //the combined distance of both trips in miles equals the sum of the
                                                    //individual distances
    
    //printing the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //prints the distance of trip 1 in miles
	  System.out.println("Trip 2 was "+distanceTrip2+" miles"); //prints the distance of trip 2 in miles
	  System.out.println("The total distance was "+totalDistance+" miles"); //prints the total distance of both trips in miles
  }                                                 //end of main method
}                                                   //end of class
