//Hannah Fabian
//Professor Chen
//CSE2-S19
//16 April 2019
//
//This program will

import java.util.Scanner; //imports the existing Scanner class
import java.util.Random; //imports the existing Random class

public class ArrayGames{ //public class we are in
   
   //method to generate an array with a random size
   public static int[] generate(int mySize){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      int[] myArray=new int[mySize]; //declares an array with a length of 5
      for (int i=0; i<mySize; i++){ //for each index of the array
         myArray[i]=randomGenerator.nextInt(20); //determine a winning number
      } //end of for loop
      return myArray; //returns myArray for use in other methods
   } //end of generate method
   
   //method for printing the input array
   public static void print(int[] myFirstArray, int[] mySecondArray, int mySize){
      System.out.print("Input 1: { "); //prints the beginning of input 1
      for (int i=0; i<mySize; i++){ //for all indexes less than mySize
         System.out.print(myFirstArray[i]+" "); //print the array
      } //end of for loop
      System.out.print('}'); //prints the end of the first array
      System.out.println(); //skip a line
      System.out.print("Input 2: { "); //prints the beginning of input 2
      for (int i=0; i<mySize; i++){ //for all indexes less than mySize
         System.out.print(mySecondArray[i]+" "); //print the array
      } //end of for loop
      System.out.print('}'); //prints the end of the second array
   } //end of print method
   
   
   //method for producing a new array
   public static void insert(int[] myFirstArray, int[] mySecondArray, int mySize){
      System.out.println(); //skip a line
      System.out.print("Output: { "); //prints the beginning of the output array
      for (int i=0; i<mySize; i++){ //for all indexes less than mySize
        System.out.print(myFirstArray[i]+" "); //print the array
      } //end of for loop
      for (int i=0; i<mySize; i++){ //for all indexes less that mySize
           System.out.print(mySecondArray[i]+" "); //print the array
      } //end of for loop
      System.out.print('}'); //prints the end of the output array
   }
   
   
   //method for shortening the array
   public static void shorten(int[] myFirstArray, int mySize){
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Enter an integer: "); //prompts user to input type of search
      int myInt=myScanner.nextInt(); //declares and initializes myInt from the scanner
      System.out.println(); //skips a line
      System.out.print("Input 1: { "); //prints the beginning of input 1
      for (int i=0; i<mySize; i++){ //for all indexes less than mySize
         System.out.print(myFirstArray[i]+" "); //print the array
      } //end of for loop
      System.out.print('}'); //prints the end of the array
      System.out.println(); //skips a line
      System.out.print("Input 2: "+myInt); //prints the bveginning of input 2
      if (myInt>mySize || myInt<=0){ //if the integer is out of range
         System.out.println(); //skip a line
         System.out.print("Output: { "); //print the beginning of the output array
         for (int i=0; i<mySize; i++){ //for all indexes less than mySize
            System.out.print(myFirstArray[i]+" "); //print the array
         } //end of for loop
      } //end of if statement
      else { //if anything else
         System.out.println(); //skip a line
         System.out.print("Output: { "); //print the beginning of the output array
         for (int i=0; i<mySize; i++){ //for all indexes less than mySize-1
            if (myInt==i){ //if the input integer is the index
               continue; //continue
            } //end of if statement
            else{ //if else
            System.out.print(myFirstArray[i]+" "); //print the array
            } //end of else statement
         } //end of for loop
      } //end of else statement
      System.out.print('}'); //prints end of output array
   } //end of shorten method
      
   //main method
   public static void main(String[] args){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      int mySize=randomGenerator.nextInt(20-10)+10; //declares and initializes the size of a matrix
      int[] myFirstArray=generate(mySize); //declares myFirstArray from generate method
      int[] mySecondArray=generate(mySize); //declares mySecondArray from generate method
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Would you like to run insert or shorten? "); //prompts user to input type of search
      String insert="insert"; //declares and initializes linear
      String shorten="shorten"; //declares and initializes binary
      String myChoice=myScanner.next(); //accept the user input data as a String
      if (myChoice.equals(insert)){ //if the input is valid
         System.out.println(); //skip a line
         print(myFirstArray, mySecondArray, mySize); //print the arrays
         insert(myFirstArray, mySecondArray, mySize); //call the insert method
         System.out.println(); //skip a line
      }
      else if (myChoice.equals(shorten)){ //if the input is valid
         shorten(myFirstArray, mySize); //call the shorten method
         System.out.println(); //skip a line
      }
      System.out.println(); //skip a line
   } //end of main method
   
} //end of public class