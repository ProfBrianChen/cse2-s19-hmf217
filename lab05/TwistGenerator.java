//Hannah Fabian
//1 March 2019
//CSE2-S19
//
//This program will use while loops to print a simple twist pattern on the screen.
//
import java.util.Scanner;                         //imports the existing Scanner class
public class TwistGenerator{                      //states the public class we are in
  public static void main(String[] args){         //main method required for every Java program
    Scanner myScanner=new Scanner(System.in);     //declares and constructs a Scanner that is ready to accept input
    
    //input statements
    System.out.print("Enter an integer: ");       //prompts the user to input an integer
    int myInt=0;                                  //declares and initializes the input as an integer
    boolean isAnInteger=myScanner.hasNextInt();   //determines if the input is an integer
    if (isAnInteger==true){                       //if the input is an integer
      myInt=myScanner.nextInt();                  //accept the user input data as an integer
      while (myInt<0){                            //while the integer is negative
        System.out.print("Enter an integer: ");   //prompts the user to input an integer
        isAnInteger=myScanner.hasNextInt();       //determines if input is an integer
        if (isAnInteger==true){                   //if the input is an integer
          myInt=myScanner.nextInt();              //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (isAnInteger==false){                 //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.print("Enter an integer: ");   //prompts the user to input an integer
        isAnInteger=myScanner.hasNextInt();       //determines if input is an integer
        if (isAnInteger==true){                   //if the input is an integer
          myInt=myScanner.nextInt();              //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //first line print statements
    int counter1=0;                               //declares and initializes the first counter
    while (counter1<(myInt/3)){                   //while the counter is less than the integer divided by 3
      System.out.print("\\ /");                   //print first part of pattern
      counter1=++counter1;                        //increment the counter
    }                                             //end of while statement
    int remainder=myInt%3;                        //initializes and declares the remainder as an integer
    if (remainder==1 || remainder==2){            //if the remainder equals 1 or 2
      System.out.print("\\");                     //print a part of the twist pattern
    }                                             //end of if statement
    System.out.println("");                       //puts next print statements on the new line
        
    //second line print statements
    int counter2=0;                               //declares and initializes the second counter
    while (counter2<(myInt/3)){                   //while the counter is less than the integer divided by 3
    System.out.print(" x ");                      //print second part of pattern
    counter2=++counter2;                          //increment the counter
    }                                             //end of while statement
    if (remainder==2){                            //if the remainder equals 1
        System.out.print(" x");                   //print a part of the twist pattern
    }                                             //end of if statement
    System.out.println("");                       //puts next print statements on the new line
        
    //third line print statements 
    int counter3=0;                               //declares and initializes the third counter
    while (counter3<(myInt/3)){                   //while the counter is less than the integer
    System.out.print("/ \\");                 //print second part of pattern
       counter3=++counter3;                      //increment the counter
        }                                         //end of while statement
    if (remainder==1 || remainder==2){            //if the remainder equals 1 or 2
    System.out.print("/");                        //print a part of the twist pattern
    }                                             //end of if statement
    System.out.println("");                       //puts next print statements on a new line
  }                                               //end of main method
}                                                 //end of public class