//Hannah Fabian
//Professor Chen
//CSE2-S19
//3 May 2019

import java.util.Arrays;

public class Lab11{
	public static void main(String[] args) {
	   int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	   int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	   System.out.println("OUTPUT:");
      int iterBest = selectionSort(myArrayBest);
	   System.out.println("The total number of operations performed on the sorted array: " + iterBest);
      System.out.println();
  	   System.out.println("OUTPUT:");
	   int iterWorst = selectionSort(myArrayWorst);
	   System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}

   //method for performing selection sort
   public static int selectionSort(int[] list) { 
      int iterations = 0;
      for (int i = 0; i < list.length - 1; i++) {
         System.out.println(Arrays.toString(list));
	      iterations++;
	     // int currentMin=list[i]; 
	      int currentMinIndex=i;
	      for (int j=i+1; j<list.length; j++) { 
            if (list[j]<list[currentMinIndex]){
               currentMinIndex=j;
            }
            iterations++;
	      }
         if (currentMinIndex != i){ 
            int temp=list[currentMinIndex];
            list[currentMinIndex]=list[i];
            list[i]=temp;
	      }
	   }
	   return iterations;
	}
}
