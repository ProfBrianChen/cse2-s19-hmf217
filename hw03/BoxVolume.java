//Hannah Fabian
//12 February 2019
//CSE2-S19
//
//This program will use the Scanner class to collect input data about the dimensions of a box 
//and calculate the volume inside the box.
//
import java.util.Scanner;                           //imports the existing Scanner class
public class BoxVolume{                             //states the public class we are in
  public static void main(String[] args){           //main method required for every Java program
    Scanner myScanner=new Scanner(System.in);       //declares and constructs a Scanner that is ready to accept input
    //input statements
    System.out.print("The length of the box is: "); //prompts the user to input the length of the box
    double length=myScanner.nextDouble();           //accepts the user input data of length of the box as a double
    System.out.print("The width of the box is: ");  //prompts the user to input the width of the box
    double width=myScanner.nextDouble();            //accepts the user input data of width of the box as a double
    System.out.print("The height of the box is: "); //prompts the user to input the height of the box
    double height=myScanner.nextDouble();           //accepts the user input data of height of the box as a double
    
    //intermediate variables and output data
    double volume=length*width*height;              //declares and assigns volume as a double by multiplying the dimensions
    int whole;                                      //declares the volume as an integer (left of decimal point)
    int tenths;                                     //declares the number in the tenths place as an integer
    int hundredths;                                 //declares the number in the hundredths place as an integer
    whole=(int)volume;                              //explicitly casts the volume as an integer
    tenths=(int)(volume*10)%10;                     //explicitly casts the number in the tenths place as an integer
    hundredths=(int)(volume*100)%10;                //explicitly casts the number in the hundredths place as an integer
    System.out.println("The volume of the box is: " +whole+ '.' +tenths+hundredths); //prints the calculated volume
    
  }                                                 //end of main method
}                                                   //end of class