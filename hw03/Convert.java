//Hannah Fabian
//12 February 2019
//CSE2-S19
//
//This program will use the Scanner class to collect input data in meters then convert the value into inches.
//
import java.util.Scanner;                           //imports the existing Scanner class
public class Convert{                               //states the public class we are in
  public static void main(String[] args){           //main method required for every Java program
    Scanner myScanner=new Scanner(System.in);       //declares and constructs a Scanner that is ready to accept input
    //input statements
    System.out.print("Enter the length in meters in the form xx.xx: "); //prompts the user to input the length in meters
    double lengthMeters=myScanner.nextDouble();     //accepts the user input data of length in meters as a double
    
    //intermediate variables and output data
    double lengthInches=lengthMeters*39.3701;       //declares and assigns the length in inches by multiplying the number of
                                                    //meters by the number of inches in one meter as a double
    int whole;                                      //declares the number of inches as an integer (left of decimal point)
    int tenths;                                     //declares the number in the tenths place as an integer
    int hundredths;                                 //declares the number in the hundredths place as an integer
    int thousandths;                                //declares the nmber in the thousandths place as an integer
    int tenthousandths;                             //declares the number in the tenthousandths place as an integer
    whole=(int)lengthInches;                        //explicitly casts the number of inches as an integer
    tenths=(int)(lengthInches*10)%10;               //explicitly casts the number in the tenths place as an integer
    hundredths=(int)(lengthInches*100)%10;          //explicitly casts the number in the hundredths place as an integer
    thousandths=(int)(lengthInches*1000)%10;        //explicitly casts the number in the thousandths place as an integer
    tenthousandths=(int)(lengthInches*10000)%10;    //explicitly casts the number in the tenthousandths place as an integer
    System.out.println(+lengthMeters+ " meters is " +whole+ '.' +tenths+hundredths+thousandths+tenthousandths+ " inches.");
                                                    //prints the length converted from meters to inches
    
  }                                                 //end of main method
}                                                   //end of class