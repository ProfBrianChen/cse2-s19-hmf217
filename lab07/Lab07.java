//Hannah Fabian
//CSE2-S19
//22 March 2019
//
//This program will use methods in order to generate "mad lib" sentences. The four methods will correspond to
//adjectives, non-primary nouns (subject), past tense verbs, and non-primary nouns (objects). We will use a 
//random number generator to randomly select a word.
//
import java.util.Random; //imports the existing Random class
import java.util.Scanner; //imports the existing Scanner class
public class Lab07{ //states the public class we are in
  
  //adjective method
  public static String adj(){ //constructs adj method
    Random randomGenerator=new Random(); //declares and constructs a Random() object
    int randomInt=randomGenerator.nextInt(31); //picks a random integer between 1 and 10
    String myAdj=""; //declares and initializes myAdj
    switch (randomInt){ //initializes switch statement
      case 0: //if the random integer is 1
        myAdj="sizzling"; //print this adj
        break; //break out of the switch statement
      case 1: //if the random integer is 1
        myAdj="sarcastic"; //print this adj
        break; //break out of the switch statement
      case 2: //if the random integer is 2
        myAdj="sassy"; //print this adj
        break; //break out of the switch statement
      case 3: //if the random integer is 3
        myAdj="shy"; //print this adj
        break; //break out of the switch statement
      case 4: //if the random integer is 4
        myAdj="sincere"; //print this adj
        break; //break out of the switch statement
      case 5: //if the random integer is 5
        myAdj="stylish"; //print this adj
        break; //break out of the switch statement
      case 6: //if the random integer is 6
        myAdj="skinny"; //print this adj
        break; //break out of the switch statement
      case 7: //if the random integer is 7
        myAdj="sleepy"; //print this adj
        break; //break out of the switch statement
      case 8: //if the random integer is 8
        myAdj="smart"; //print this adj
        break; //break out of the switch statement
      case 9: //if the random integer is 9
        myAdj="sneaky"; //print this adj
        break; //break out of the switch statement
      case 10: //if the random integer is 10
        myAdj="sweet"; //print this adj
        break; //break out of the switch statement
      case 11: //if the random integer is 11
        myAdj="salty"; //print this adj
        break; //break out of the switch statement
      case 12: //if the random integer is 12
        myAdj="serendipitous"; //print this adj
        break; //break out of the switch statement
      case 13: //if the random integer is 13
        myAdj="smelly"; //print this adj
        break; //break out of the switch statement
      case 14: //if the random integer is 14
        myAdj="stinky"; //print this adj
        break; //break out of the switch statement
      case 15: //if the random integer is 15
        myAdj="Scandinavian"; //print this adj
        break; //break out of the switch statement
      case 16: //if the random integer is 16
        myAdj="sappy"; //print this adj
        break; //break out of the switch statement
      case 17: //if the random integer is 17
        myAdj="scary"; //print this adj
        break; //break out of the switch statement
      case 18: //if the random integer is 18
        myAdj="scholarly"; //print this adj
        break; //break out of the switch statement
      case 19: //if the random integer is 19
        myAdj="scurvy"; //print this adj
        break; //break out of the switch statement
      case 20: //if the random integer is 20
        myAdj="seasick"; //print this adj
        break; //break out of the switch statement
      case 21: //if the random integer is 21
        myAdj="sensitive"; //print this adj
        break; //break out of the switch statement
      case 22: //if the random integer is 22
        myAdj="shaken"; //print this adj
        break; //break out of the switch statement
      case 23: //if the random integer is 23
        myAdj="shamed"; //print this adj
        break; //break out of the switch statement
      case 24: //if the random integer is 24
        myAdj="shoeless"; //print this adj
        break; //break out of the switch statement
      case 25: //if the random integer is 25
        myAdj="shrunken"; //print this adj
        break; //break out of the switch statement
      case 26: //if the random integer is 26
        myAdj="Sicilian"; //print this adj
        break; //break out of the switch statement
      case 27: //if the random integer is 27
        myAdj="slaphappy"; //print this adj
        break; //break out of the switch statement
      case 28: //if the random integer is 28
        myAdj="slimy"; //print this adj
        break; //break out of the switch statement
      case 29: //if the random integer is 29
        myAdj="sly"; //print this adj
        break; //break out of the switch statement
      case 30: //if the random integer is 30
        myAdj="squinting"; //print this adj
        break; //break out of the switch statement
    } //end of switch statement
    return myAdj; //returns myAdj
  } //end of adj method
  
  //subject (nouns) method
  public static String sub(){ //constructs adj method
    Random randomGenerator=new Random(); //declares and constructs a Random() object
    int randomInt=randomGenerator.nextInt(10); //picks a random integer between 1 and 10
    String mySub=""; //declares and initializes mySub
    switch (randomInt){ //initializes switch statement
      case 0: //if the random integer is 0
        mySub="saltwater crocodile"; //print this sub
        break; //break out of the switch statement
      case 1: //if the random integer is 1
        mySub="saber-toothed tiger"; //print this sub
        break; //break out of the switch statement
      case 2: //if the random integer is 2
        mySub="seal"; //print this sub
        break; //break out of the switch statement
      case 3: //if the random integer is 3
        mySub="Saint Bernard"; //print this sub
        break; //break out of the switch statement
      case 4: //if the random integer is 4
        mySub="salamander"; //print this sub
        break; //break out of the switch statement
      case 5: //if the random integer is 5
        mySub="scorpion"; //print this sub
        break; //break out of the switch statement
      case 6: //if the random integer is 6
        mySub="sheep"; //print this sub
        break; //break out of the switch statement
      case 7: //if the random integer is 7
        mySub="sea lion"; //print this sub
        break; //break out of the switch statement
      case 8: //if the random integer is 8
        mySub="sea otter"; //print this sub
        break; //break out of the switch statement
      case 9: //if the random integer is 9
        mySub="sea slug"; //print this sub
        break; //break out of the switch statement
      case 10: //if the random integer is 10
        mySub="seahorse"; //print this sub
        break; //break out of the switch statement
    } //end of switch statement
    return mySub; //returns mySub
  } //end of sub method
  

  //verb method
  public static String verb(){ //constructs adj method
    Random randomGenerator=new Random(); //declares and constructs a Random() object
    int randomInt=randomGenerator.nextInt(31); //picks a random integer between 1 and 10
    String myVerb=""; //declares and initializes myVerb
    switch (randomInt){ //initializes switch statement
      case 0: //if the random integer is 0
        myVerb="sabotaged"; //print this verb
        break; //break out of the switch statement
      case 1: //if the random integer is 1
        myVerb="sailed past"; //print this verb
        break; //break out of the switch statement
      case 2: //if the random integer is 2
        myVerb="saved"; //print this verb
        break; //break out of the switch statement
      case 3: //if the random integer is 3
        myVerb="scared"; //print this verb
        break; //break out of the switch statement
      case 4: //if the random integer is 4
        myVerb="sang to"; //print this verb
        break; //break out of the switch statement
      case 5: //if the random integer is 5
        myVerb="scrutinized"; //print this verb
        break; //break out of the switch statement
      case 6: //if the random integer is 6
        myVerb="searched"; //print this verb
        break; //break out of the switch statement
      case 7: //if the random integer is 7
        myVerb="seized"; //print this verb
        break; //break out of the switch statement
      case 8: //if the random integer is 8
        myVerb="served suffering succotash to"; //print this verb
        break; //break out of the switch statement
      case 9: //if the random integer is 9
        myVerb="shaved"; //print this verb
        break; //break out of the switch statement
      case 10: //if the random integer is 10
        myVerb="saw"; //print this verb
        break; //break out of the switch statement
      case 11: //if the random integer is 11
        myVerb="sabered"; //print this verb
        break; //break out of the switch statement
      case 12: //if the random integer is 12
        myVerb="saddled"; //print this verb
        break; //break out of the switch statement
      case 13: //if the random integer is 13
        myVerb="safeguarded"; //print this verb
        break; //break out of the switch statement
      case 14: //if the random integer is 14
        myVerb="sanctioned"; //print this verb
        break; //break out of the switch statement
      case 15: //if the random integer is 15
        myVerb="sandbagged"; //print this verb
        break; //break out of the switch statement
      case 16: //if the random integer is 16
        myVerb="sandwiched"; //print this verb
        break; //break out of the switch statement
      case 17: //if the random integer is 17
        myVerb="sassed"; //print this verb
        break; //break out of the switch statement
      case 18: //if the random integer is 18
        myVerb="said farewell to"; //print this verb
        break; //break out of the switch statement
      case 19: //if the random integer is 19
        myVerb="scalded"; //print this verb
        break; //break out of the switch statement
      case 20: //if the random integer is 20
        myVerb="see-sawed with"; //print this verb
        break; //break out of the switch statement
      case 21: //if the random integer is 21
        myVerb="sold out"; //print this verb
        break; //break out of the switch statement
      case 22: //if the random integer is 22
        myVerb="sheltered"; //print this verb
        break; //break out of the switch statement
      case 23: //if the random integer is 23
        myVerb="shimmied past"; //print this verb
        break; //break out of the switch statement
      case 24: //if the random integer is 24
        myVerb="shipwrecked"; //print this verb
        break; //break out of the switch statement
      case 25: //if the random integer is 25
        myVerb="shot"; //print this verb
        break; //break out of the switch statement
      case 26: //if the random integer is 26
        myVerb="shmoozed"; //print this verb
        break; //break out of the switch statement
      case 27: //if the random integer is 27
        myVerb="seized"; //print this verb
        break; //break out of the switch statement
      case 28: //if the random integer is 28
        myVerb="served suffering succotash"; //print this verb
        break; //break out of the switch statement
      case 29: //if the random integer is 29
        myVerb="shot the breeze with"; //print this verb
        break; //break out of the switch statement
      case 30: //if the random integer is 30
        myVerb="short-changed"; //print this verb
        break; //break out of the switch statement
    } //end of switch statement
    return myVerb; //returns myVerb
  } //end of verb method
  
  //object (nouns) method
  public static String obj(){ //constructs adj method
    Random randomGenerator=new Random(); //declares and constructs a Random() object
    int randomInt=randomGenerator.nextInt(11); //picks a random integer between 1 and 10
    String myObj=""; //declares and initializes myObj
    switch (randomInt){ //initializes switch statement
      case 0: //if the random integer is 0
        myObj="salmon"; //print this obj
        break; //break out of the switch statement
      case 1: //if the random integer is 1
        myObj="Shih Tzu"; //print this obj
        break; //break out of the switch statement
      case 2: //if the random integer is 2
        myObj="shrimp"; //print this obj
        break; //break out of the switch statement
      case 3: //if the random integer is 3
        myObj="sand lizard"; //print this obj
        break; //break out of the switch statement
      case 4: //if the random integer is 4
        myObj="Siamese cat"; //print this obj
        break; //break out of the switch statement
      case 5: //if the random integer is 5
        myObj="Siamese fighting fish"; //print this obj
        break; //break out of the switch statement
      case 6: //if the random integer is 6
        myObj="skunk"; //print this obj
        break; //break out of the switch statement
      case 7: //if the random integer is 7
        myObj="sloth"; //print this obj
        break; //break out of the switch statement
      case 8: //if the random integer is 8
        myObj="snake"; //print this obj
        break; //break out of the switch statement
      case 9: //if the random integer is 9
        myObj="snapping turtle"; //print this obj
        break; //break out of the switch statement
      case 10: //if the random integer is 10
        myObj="snowy owl"; //print this obj
        break; //break out of the switch statement
    } //end of switch statement
    return myObj; //returns myObj
  } //end of obj method  
  
  //adverbs method
  public static String adv(){ //constructs adj method
    Random randomGenerator=new Random(); //declares and constructs a Random() object
    int randomInt=randomGenerator.nextInt(30); //picks a random integer between 1 and 10
    String myAdv=""; //declares and initializes myAdv
    switch (randomInt){ //initializes switch statement
      case 0: //if the random integer is 0
        myAdv="sadly"; //print this adv
        break; //break out of the switch statement
      case 1: //if the random integer is 1
        myAdv="safely"; //print this adv
        break; //break out of the switch statement
      case 2: //if the random integer is 2
        myAdv="satisfyingly"; //print this adv
        break; //break out of the switch statement
      case 3: //if the random integer is 3
        myAdv="savagely"; //print this adv
        break; //break out of the switch statement
      case 4: //if the random integer is 4
        myAdv="scandalously"; //print this adv
        break; //break out of the switch statement
      case 5: //if the random integer is 5
        myAdv="secretly"; //print this adv
        break; //break out of the switch statement
      case 6: //if the random integer is 6
        myAdv="seriously"; //print this adv
        break; //break out of the switch statement
      case 7: //if the random integer is 7
        myAdv="seductively"; //print this adv
        break; //break out of the switch statement
      case 8: //if the random integer is 8
        myAdv="selfishly"; //print this adv
        break; //break out of the switch statement
      case 9: //if the random integer is 9
        myAdv="semianually"; //print this adv
        break; //break out of the switch statement
      case 10: //if the random integer is 10
        myAdv="simply"; //print this adv
        break; //break out of the switch statement
      case 11: //if the random integer is 11
        myAdv="sacredly"; //print this adv
        break; //break out of the switch statement
      case 12: //if the random integer is 12
        myAdv="sagaciously"; //print this adv
        break; //break out of the switch statement
      case 13: //if the random integer is 13
        myAdv="sanely"; //print this adv
        break; //break out of the switch statement
      case 14: //if the random integer is 14
        myAdv="satirically"; //print this adv
        break; //break out of the switch statement
      case 15: //if the random integer is 15
        myAdv="saucily"; //print this adv
        break; //break out of the switch statement
      case 16: //if the random integer is 16
        myAdv="scarily"; //print this adv
        break; //break out of the switch statement
      case 17: //if the random integer is 17
        myAdv="scathingly"; //print this adv
        break; //break out of the switch statement
      case 18: //if the random integer is 18
        myAdv="sceptically"; //print this adv
        break; //break out of the switch statement
      case 19: //if the random integer is 19
        myAdv="scoffingly"; //print this adv
        break; //break out of the switch statement
      case 20: //if the random integer is 20
        myAdv="scornfully"; //print this adv
        break; //break out of the switch statement
      case 21: //if the random integer is 21
        myAdv="scrumptiously"; //print this adv
        break; //break out of the switch statement
      case 22: //if the random integer is 22
        myAdv="senselessly"; //print this adv
        break; //break out of the switch statement
      case 23: //if the random integer is 23
        myAdv="severely"; //print this adv
        break; //break out of the switch statement
      case 24: //if the random integer is 24
        myAdv="shallowly"; //print this adv
        break; //break out of the switch statement
      case 25: //if the random integer is 25
        myAdv="shamefully"; //print this adv
        break; //break out of the switch statement
      case 26: //if the random integer is 26
        myAdv="shockingly"; //print this adv
        break; //break out of the switch statement
      case 27: //if the random integer is 27
        myAdv="shoddily"; //print this adv
        break; //break out of the switch statement
      case 28: //if the random integer is 28
        myAdv="shudderingly"; //print this adv
        break; //break out of the switch statement
      case 29: //if the random integer is 29
        myAdv="sidesplittingly"; //print this adv
        break; //break out of the switch statement
    } //end of switch statement
    return myAdv;  //returns myAdv
  } //end of adv method  
  
  //determining if the input is valid method
  public static int valid(){ //constructs valid method
    Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
    System.out.print("Would you like to hear more? Enter 1 for yes or 2 for no: ");
    int myInt=1; //declares and initializes the input as an integer
    boolean isAnInteger=myScanner.hasNextInt(); //determines if the input is an integer
    if (isAnInteger==true){ //if the input is an integer
      myInt=myScanner.nextInt(); //accept the user input data as an integer
      while (myInt<=0 || myInt>=3){ //while the integer is out of range
        System.out.println("ERROR: Input out of range."); //displays error message
        System.out.print("Would you like to hear more? Enter 1 for yes or 2 for no: ");
        isAnInteger=myScanner.hasNextInt(); //determines if input is an integer
        if (isAnInteger==true){ //if the input is an integer
          myInt=myScanner.nextInt(); //accept the user input data as an integer
        } //end of if statement
      } //end of while loop
    } //end of if statement
    else{ //if the input is not an integer
      while (isAnInteger==false){ //while the input is not an integer
        String junkWord = myScanner.next(); //flush out what is in the Scanner
        System.out.println("ERROR: Input is not an integer."); //displays error message
        System.out.print("Would you like to hear more? Enter 1 for yes or 2 for no: ");
        isAnInteger=myScanner.hasNextInt(); //determines if input is an integer
        if (isAnInteger==true){ //if the input is an integer
          myInt=myScanner.nextInt(); //accept the user input data as an integer
        } //end of if statement
      } //end of while loop
    } //end of else statement
    return myInt;
  } //end of valid method
  
  //main method
  public static void main(String[] args){ //main method required for every Java program
    //first sentence
    int myInt=1; //declares and initializes myInt in this method
    String myAdj1=adj(); //declares and initializes the variable from other method
    String myAdj2=adj(); //declares and initializes the variable from other method
    String myAdj3=adj(); //declares and initializes the variable from other method
    String myVerb=verb(); //declares and initializes the variable from other method
    String myObj=obj(); //declares and initializes the variable from other method
    String mySub=sub(); //declares and initializes the variable from other method
    String myAdv=adv(); //declares and initializes the variable from other method
    System.out.println("First, the "+myAdj1+", "+myAdj2+" "+mySub+" "+myVerb+" the "+myAdj3+" "+myObj+'.'); //print this
    myInt=valid(); //determines value of myInt (i.e. continue story?)
      
    //second sentence
    Random randomGenerator=new Random(); //declares and constructs a Random() object
    int mySupport=randomGenerator.nextInt(5); //decides how many supporting sentences to print
    for (; mySupport>=0; mySupport--){ //for this many sentences
        if (myInt==1){ //if user selected 1
        String myAdj=adj(); //create a new adjective 
        myAdv=adv(); //create a new adverb
        myVerb=verb(); //create a new verb
        System.out.println("It was "+myAdv+" "+myAdj+" to the "+myAdj3+" "+myObj+"."); //print this
      } //end of if statement
    } //end of for loop
    
    //concluding sentence
    if (myInt==1){ //if user selected 1
      myVerb=verb(); //create a new verb
      System.out.println("Finally, the "+mySub+" "+myVerb+" the "+myObj+'!'); //print this
    } //end of if statement
  } //end of main method
} //end of public class