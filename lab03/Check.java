//Hannah Fabian
//8 February 2019
//CSE2-S19
//
//This program will use the Scanner class to calculate the cost of a dinner,
//the percentage tip, the number ways the check should be split, and the 
//final amount each person at the dinner must pay.
//
import java.util.Scanner;                           //imports the existing Scanner class
public class Check{                               //states the public class we are in
  public static void main(String[] args){           //main method required for every Java program
    Scanner myScanner=new Scanner(System.in);       //declares and constructs a Scanner that is ready to accept input
    //input statements
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompts the user to input the original cost
                                                    //of the check
    double checkCost=myScanner.nextDouble();        //accepts the user input data of original cost as a double
    System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx: "); //prompts the user to
                                                    //input the percentage tip that they wish to pay
    double tipPercent=myScanner.nextDouble();       //accepts the user input data of tip percentage as a double
    tipPercent /=100;                               //converts the tip percentage into a decimal value
    System.out.print("Enter the number pf people who went out to dinner: "); //prompts the user to input the number of people who
                                                    //went out to dinner
    int numPeople=myScanner.nextInt();              //accepts the user input data of number of people who went as an integer
        
    //intermediate variables and output data
    double totalCost;                               //declares the total cost as a double
    double costPerPerson;                           //declares the cost per person as a double
    int dollars;                                    //declares the number of dollars as an integer (left of decimal point)
    int dimes;                                      //declares the number of dimes as an integer (tenths place)
    int pennies;                                    //declares the number of pennies as an integer (hundredths place)
    totalCost=checkCost*(1+tipPercent);             //assigns the total cost as the original cost plus the tip
    costPerPerson=totalCost/numPeople;              //assigns the cost per person as the total cost per amount of people
    dollars=(int)costPerPerson;                     //explicitly casts the number of dollars as an integer
    dimes=(int)(costPerPerson*10)%10;               //explicitly casts the number of dimes as an integer, then finds the
                                                    //remainder, which gives the amount of dimes as a whole number
    pennies=(int)(costPerPerson*100)%10;            //explicitly casts the number of pennies as an integer, then finds the 
                                                    //remainder, which gives the number of pennies as a whole number
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //prints out the amount that each
                                                    //person owes 
    
  }                                                 //end of main method
}                                                   //end of class