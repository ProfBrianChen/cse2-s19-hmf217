//Hannah Fabian
//15 February 2019
//CSE2-S19
//
//The poker hand check will pick 5 random card from a new deck of cards each.
//It will detect if there is a pari, two pair, or three of a kind.
//
public class PokerHandCheck {                       //states the public class we are in
  public static void main(String[] args){           //main method required for every Java program
    //first input statements
    int randomNumber1=(int)(Math.random()*52)+1;    //declares and assigns the random number, explicitly casted as an integer
    String cardSuit1="";                            //declares the name of the suit as a String
    String cardIdentity1="";                        //declares the identity of the card as a String
    
    //if statements determining the first card suit
    if (randomNumber1>=1 && randomNumber1<=13){     //if statement with boolean condition
      cardSuit1="diamonds";                         //assigns the card suit as diamond
    }                                               //end of if statement
    if (randomNumber1>=14 && randomNumber1<=26){    //if statement with boolean condition
      cardSuit1="clubs";                            //assigns the card suit as club
    }                                               //end of if statement
    if (randomNumber1>=27 && randomNumber1<=39){    //if statement with boolean condition
      cardSuit1="hearts";                           //assigns the card suit as heart
    }                                               //end of if statement
    if (randomNumber1>=40 && randomNumber1<=52){    //if statement with boolean condition
      cardSuit1="spades";                           //assigns the card suit as spade
    }                                               //end of if statement
    
    //switch statement determining the first card number
    switch (randomNumber1){                         //switch statement
      case 1: case 14: case 27: case 40:            //states the case values
        cardIdentity1="Ace";                        //assigns the Ace to corresponding cases
        break;                                      //stops case fallthrough
      case 2: case 15: case 28: case 41:            //states the case values
        cardIdentity1="2";                          //assigns the 2 to corresponding cases
        break;                                      //stops case fallthrough
      case 3: case 16: case 29: case 42:            //states the case values
        cardIdentity1="3";                          //assigns the 3 to corresponding cases
        break;                                      //stops case fallthrough
      case 4: case 17: case 30: case 43:            //states the case values
        cardIdentity1="4";                          //assigns the 4 to corresponding cases
        break;                                      //stops case fallthrough
      case 5: case 18: case 31: case 44:            //states the case values
        cardIdentity1="5";                          //assigns the 5 to corresponding cases
        break;                                      //stops case fallthrough
      case 6: case 19: case 32: case 45:            //states the case values
        cardIdentity1="6";                          //assigns the 6 to corresponding cases
        break;                                      //stops case fallthrough
      case 7: case 20: case 33: case 46:            //states the case values
        cardIdentity1="7";                          //assigns the 7 to corresponding cases
        break;                                      //stops case fallthrough
      case 8: case 21: case 34: case 47:            //states the case values
        cardIdentity1="8";                          //assigns the 8 to corresponding cases
        break;                                      //stops case fallthrough
      case 9: case 22: case 35: case 48:            //states the case values
        cardIdentity1="9";                          //assigns the 9 to corresponding cases
        break;                                      //stops case fallthrough
      case 10: case 23: case 36: case 49:           //states the case values
        cardIdentity1="10";                         //assigns the 10 to corresponding cases
        break;                                      //stops case fallthrough
      case 11: case 24: case 37: case 50:           //states the case values
        cardIdentity1="Jack";                       //assigns the Jack to corresponding cases
        break;                                      //stops case fallthrough
      case 12: case 25: case 38: case 51:           //states the case values
        cardIdentity1="Queen";                      //assigns the Queen to corresponding cases
        break;                                      //stops case fallthrough
      case 13: case 26: case 39: case 52:           //states the case values
        cardIdentity1="King";                       //assigns the King to corresponding cases
        break;                                      //stops case fallthrough
    }                                               //end of switch statement
    
    //second input statements
    int randomNumber2=(int)(Math.random()*52)+1;    //declares and assigns the random number, explicitly casted as an integer
    String cardSuit2="";                            //declares the name of the suit as a String
    String cardIdentity2="";                        //declares the identity of the card as a String
    
    //if statements determining the second card suit
    if (randomNumber2>=1 && randomNumber2<=13){     //if statement with boolean condition
      cardSuit2="diamonds";                         //assigns the card suit as diamond
    }                                               //end of if statement
    if (randomNumber2>=14 && randomNumber2<=26){    //if statement with boolean condition
      cardSuit2="clubs";                            //assigns the card suit as club
    }                                               //end of if statement
    if (randomNumber2>=27 && randomNumber2<=39){    //if statement with boolean condition
      cardSuit2="hearts";                           //assigns the card suit as heart
    }                                               //end of if statement
    if (randomNumber2>=40 && randomNumber2<=52){    //if statement with boolean condition
      cardSuit2="spades";                           //assigns the card suit as spade
    }                                               //end of if statement
    
    //switch statement determining the second card number
    switch (randomNumber2){                         //switch statement
      case 1: case 14: case 27: case 40:            //states the case values
        cardIdentity2="Ace";                        //assigns the Ace to corresponding cases
        break;                                      //stops case fallthrough
      case 2: case 15: case 28: case 41:            //states the case values
        cardIdentity2="2";                          //assigns the 2 to corresponding cases
        break;                                      //stops case fallthrough
      case 3: case 16: case 29: case 42:            //states the case values
        cardIdentity2="3";                          //assigns the 3 to corresponding cases
        break;                                      //stops case fallthrough
      case 4: case 17: case 30: case 43:            //states the case values
        cardIdentity2="4";                          //assigns the 4 to corresponding cases
        break;                                      //stops case fallthrough
      case 5: case 18: case 31: case 44:            //states the case values
        cardIdentity2="5";                          //assigns the 5 to corresponding cases
        break;                                      //stops case fallthrough
      case 6: case 19: case 32: case 45:            //states the case values
        cardIdentity2="6";                          //assigns the 6 to corresponding cases
        break;                                      //stops case fallthrough
      case 7: case 20: case 33: case 46:            //states the case values
        cardIdentity2="7";                          //assigns the 7 to corresponding cases
        break;                                      //stops case fallthrough
      case 8: case 21: case 34: case 47:            //states the case values
        cardIdentity2="8";                          //assigns the 8 to corresponding cases
        break;                                      //stops case fallthrough
      case 9: case 22: case 35: case 48:            //states the case values
        cardIdentity2="9";                          //assigns the 9 to corresponding cases
        break;                                      //stops case fallthrough
      case 10: case 23: case 36: case 49:           //states the case values
        cardIdentity2="10";                         //assigns the 10 to corresponding cases
        break;                                      //stops case fallthrough
      case 11: case 24: case 37: case 50:           //states the case values
        cardIdentity2="Jack";                       //assigns the Jack to corresponding cases
        break;                                      //stops case fallthrough
      case 12: case 25: case 38: case 51:           //states the case values
        cardIdentity2="Queen";                      //assigns the Queen to corresponding cases
        break;                                      //stops case fallthrough
      case 13: case 26: case 39: case 52:           //states the case values
        cardIdentity2="King";                       //assigns the King to corresponding cases
        break;                                      //stops case fallthrough
    }                                               //end of switch statement
    
    //third input statements
    int randomNumber3=(int)(Math.random()*52)+1;    //declares and assigns the random number, explicitly casted as an integer
    String cardSuit3="";                            //declares the name of the suit as a String
    String cardIdentity3="";                        //declares the identity of the card as a String
    
    //if statements determining the third card suit
    if (randomNumber3>=1 && randomNumber3<=13){     //if statement with boolean condition
      cardSuit3="diamonds";                         //assigns the card suit as diamond
    }                                               //end of if statement
    if (randomNumber3>=14 && randomNumber3<=26){    //if statement with boolean condition
      cardSuit3="clubs";                            //assigns the card suit as club
    }                                               //end of if statement
    if (randomNumber3>=27 && randomNumber3<=39){    //if statement with boolean condition
      cardSuit3="hearts";                           //assigns the card suit as heart
    }                                               //end of if statement
    if (randomNumber3>=40 && randomNumber3<=52){    //if statement with boolean condition
      cardSuit3="spades";                           //assigns the card suit as spade
    }                                               //end of if statement
    
    //switch statement determining the third card number
    switch (randomNumber3){                         //switch statement
      case 1: case 14: case 27: case 40:            //states the case values
        cardIdentity3="Ace";                        //assigns the Ace to corresponding cases
        break;                                      //stops case fallthrough
      case 2: case 15: case 28: case 41:            //states the case values
        cardIdentity3="2";                          //assigns the 2 to corresponding cases
        break;                                      //stops case fallthrough
      case 3: case 16: case 29: case 42:            //states the case values
        cardIdentity3="3";                          //assigns the 3 to corresponding cases
        break;                                      //stops case fallthrough
      case 4: case 17: case 30: case 43:            //states the case values
        cardIdentity3="4";                          //assigns the 4 to corresponding cases
        break;                                      //stops case fallthrough
      case 5: case 18: case 31: case 44:            //states the case values
        cardIdentity3="5";                          //assigns the 5 to corresponding cases
        break;                                      //stops case fallthrough
      case 6: case 19: case 32: case 45:            //states the case values
        cardIdentity3="6";                          //assigns the 6 to corresponding cases
        break;                                      //stops case fallthrough
      case 7: case 20: case 33: case 46:            //states the case values
        cardIdentity3="7";                          //assigns the 7 to corresponding cases
        break;                                      //stops case fallthrough
      case 8: case 21: case 34: case 47:            //states the case values
        cardIdentity3="8";                          //assigns the 8 to corresponding cases
        break;                                      //stops case fallthrough
      case 9: case 22: case 35: case 48:            //states the case values
        cardIdentity3="9";                          //assigns the 9 to corresponding cases
        break;                                      //stops case fallthrough
      case 10: case 23: case 36: case 49:           //states the case values
        cardIdentity3="10";                         //assigns the 10 to corresponding cases
        break;                                      //stops case fallthrough
      case 11: case 24: case 37: case 50:           //states the case values
        cardIdentity3="Jack";                       //assigns the Jack to corresponding cases
        break;                                      //stops case fallthrough
      case 12: case 25: case 38: case 51:           //states the case values
        cardIdentity3="Queen";                      //assigns the Queen to corresponding cases
        break;                                      //stops case fallthrough
      case 13: case 26: case 39: case 52:           //states the case values
        cardIdentity3="King";                       //assigns the King to corresponding cases
        break;                                      //stops case fallthrough
    }                                               //end of switch statement
    
    //fourth input statements
    int randomNumber4=(int)(Math.random()*52)+1;    //declares and assigns the random number, explicitly casted as an integer
    String cardSuit4="";                            //declares the name of the suit as a String
    String cardIdentity4="";                        //declares the identity of the card as a String
    
    //if statements determining the fourth card suit
    if (randomNumber4>=1 && randomNumber4<=13){     //if statement with boolean condition
      cardSuit4="diamonds";                         //assigns the card suit as diamond
    }                                               //end of if statement
    if (randomNumber4>=14 && randomNumber4<=26){    //if statement with boolean condition
      cardSuit4="clubs";                            //assigns the card suit as club
    }                                               //end of if statement
    if (randomNumber4>=27 && randomNumber4<=39){    //if statement with boolean condition
      cardSuit4="hearts";                           //assigns the card suit as heart
    }                                               //end of if statement
    if (randomNumber4>=40 && randomNumber4<=52){    //if statement with boolean condition
      cardSuit4="spades";                           //assigns the card suit as spade
    }                                               //end of if statement
    
    //switch statement determining the fourth card number
    switch (randomNumber4){                         //switch statement
      case 1: case 14: case 27: case 40:            //states the case values
        cardIdentity4="Ace";                        //assigns the Ace to corresponding cases
        break;                                      //stops case fallthrough
      case 2: case 15: case 28: case 41:            //states the case values
        cardIdentity4="2";                          //assigns the 2 to corresponding cases
        break;                                      //stops case fallthrough
      case 3: case 16: case 29: case 42:            //states the case values
        cardIdentity4="3";                          //assigns the 3 to corresponding cases
        break;                                      //stops case fallthrough
      case 4: case 17: case 30: case 43:            //states the case values
        cardIdentity4="4";                          //assigns the 4 to corresponding cases
        break;                                      //stops case fallthrough
      case 5: case 18: case 31: case 44:            //states the case values
        cardIdentity4="5";                          //assigns the 5 to corresponding cases
        break;                                      //stops case fallthrough
      case 6: case 19: case 32: case 45:            //states the case values
        cardIdentity4="6";                          //assigns the 6 to corresponding cases
        break;                                      //stops case fallthrough
      case 7: case 20: case 33: case 46:            //states the case values
        cardIdentity4="7";                          //assigns the 7 to corresponding cases
        break;                                      //stops case fallthrough
      case 8: case 21: case 34: case 47:            //states the case values
        cardIdentity4="8";                          //assigns the 8 to corresponding cases
        break;                                      //stops case fallthrough
      case 9: case 22: case 35: case 48:            //states the case values
        cardIdentity4="9";                          //assigns the 9 to corresponding cases
        break;                                      //stops case fallthrough
      case 10: case 23: case 36: case 49:           //states the case values
        cardIdentity4="10";                         //assigns the 10 to corresponding cases
        break;                                      //stops case fallthrough
      case 11: case 24: case 37: case 50:           //states the case values
        cardIdentity4="Jack";                       //assigns the Jack to corresponding cases
        break;                                      //stops case fallthrough
      case 12: case 25: case 38: case 51:           //states the case values
        cardIdentity4="Queen";                      //assigns the Queen to corresponding cases
        break;                                      //stops case fallthrough
      case 13: case 26: case 39: case 52:           //states the case values
        cardIdentity4="King";                       //assigns the King to corresponding cases
        break;                                      //stops case fallthrough
    }                                               //end of switch statement
    
    //fifth input statements
    int randomNumber5=(int)(Math.random()*52)+1;    //declares and assigns the random number, explicitly casted as an integer
    String cardSuit5="";                            //declares the name of the suit as a String
    String cardIdentity5="";                        //declares the identity of the card as a String
    
    //if statements determining the fifth card suit
    if (randomNumber5>=1 && randomNumber5<=13){     //if statement with boolean condition
      cardSuit5="diamonds";                         //assigns the card suit as diamond
    }                                               //end of if statement
    if (randomNumber5>=14 && randomNumber5<=26){    //if statement with boolean condition
      cardSuit5="clubs";                            //assigns the card suit as club
    }                                               //end of if statement
    if (randomNumber5>=27 && randomNumber5<=39){    //if statement with boolean condition
      cardSuit5="hearts";                           //assigns the card suit as heart
    }                                               //end of if statement
    if (randomNumber5>=40 && randomNumber5<=52){    //if statement with boolean condition
      cardSuit5="spades";                           //assigns the card suit as spade
    }                                               //end of if statement
    
    //switch statement determining the first card number
    switch (randomNumber5){                         //switch statement
      case 1: case 14: case 27: case 40:            //states the case values
        cardIdentity5="Ace";                        //assigns the Ace to corresponding cases
        break;                                      //stops case fallthrough
      case 2: case 15: case 28: case 41:            //states the case values
        cardIdentity5="2";                          //assigns the 2 to corresponding cases
        break;                                      //stops case fallthrough
      case 3: case 16: case 29: case 42:            //states the case values
        cardIdentity5="3";                          //assigns the 3 to corresponding cases
        break;                                      //stops case fallthrough
      case 4: case 17: case 30: case 43:            //states the case values
        cardIdentity5="4";                          //assigns the 4 to corresponding cases
        break;                                      //stops case fallthrough
      case 5: case 18: case 31: case 44:            //states the case values
        cardIdentity5="5";                          //assigns the 5 to corresponding cases
        break;                                      //stops case fallthrough
      case 6: case 19: case 32: case 45:            //states the case values
        cardIdentity5="6";                          //assigns the 6 to corresponding cases
        break;                                      //stops case fallthrough
      case 7: case 20: case 33: case 46:            //states the case values
        cardIdentity5="7";                          //assigns the 7 to corresponding cases
        break;                                      //stops case fallthrough
      case 8: case 21: case 34: case 47:            //states the case values
        cardIdentity5="8";                          //assigns the 8 to corresponding cases
        break;                                      //stops case fallthrough
      case 9: case 22: case 35: case 48:            //states the case values
        cardIdentity5="9";                          //assigns the 9 to corresponding cases
        break;                                      //stops case fallthrough
      case 10: case 23: case 36: case 49:           //states the case values
        cardIdentity5="10";                         //assigns the 10 to corresponding cases
        break;                                      //stops case fallthrough
      case 11: case 24: case 37: case 50:           //states the case values
        cardIdentity5="Jack";                       //assigns the Jack to corresponding cases
        break;                                      //stops case fallthrough
      case 12: case 25: case 38: case 51:           //states the case values
        cardIdentity5="Queen";                      //assigns the Queen to corresponding cases
        break;                                      //stops case fallthrough
      case 13: case 26: case 39: case 52:           //states the case values
        cardIdentity5="King";                       //assigns the King to corresponding cases
        break;                                      //stops case fallthrough
    }                                               //end of switch statement
    
    //print statement naming the cards
    System.out.println("Your random cards were:");  //prints intro statement
    System.out.println(" ");                        //prints a blank line
    System.out.println("The " + cardIdentity1 + " of " + cardSuit1); //prints the first generated card
    System.out.println("The " + cardIdentity2 + " of " + cardSuit2); //prints the second generated card
    System.out.println("The " + cardIdentity3 + " of " + cardSuit3); //prints the second generated card
    System.out.println("The " + cardIdentity4 + " of " + cardSuit4); //prints the second generated card
    System.out.println("The " + cardIdentity5 + " of " + cardSuit5); //prints the second generated card
    System.out.println(" ");                        //prints a blank line
    
    //boolean conditions for pairs or three of a kind
    boolean pair1=cardIdentity1==cardIdentity2;     //declares and assigns a potential pair as a boolean
    boolean pair2=cardIdentity1==cardIdentity3;     //declares and assigns a potential pair as a boolean
    boolean pair3= cardIdentity1==cardIdentity4;    //declares and assigns a potential pair as a boolean
    boolean pair4=cardIdentity1==cardIdentity5;     //declares and assigns a potential pair as a boolean
    boolean pair5=cardIdentity2==cardIdentity3;     //declares and assigns a potential pair as a boolean
    boolean pair6=cardIdentity2==cardIdentity4;     //declares and assigns a potential pair as a boolean
    boolean pair7=cardIdentity2==cardIdentity5;     //declares and assigns a potential pair as a boolean
    boolean pair8=cardIdentity3==cardIdentity4;     //declares and assigns a potential pair as a boolean
    boolean pair9=cardIdentity3==cardIdentity5;     //declares and assigns a potential pair as a boolean
    boolean pair10=cardIdentity4==cardIdentity5;    //declares and assigns a potential pair as a boolean
    
    boolean threeKind1=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind2=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind3=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind4=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind5=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind6=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind7=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind8=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind9=false;                       //declares and assigns a potential three of a kind as a boolean
    boolean threeKind10=false;                      //declares and assigns a potential three of a kind as a boolean

    
    if(cardIdentity1==cardIdentity2 && cardIdentity1==cardIdentity3){ //if the three cards have the same value
      threeKind1=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity1==cardIdentity2 && cardIdentity1==cardIdentity4){ //if the three cards have the same value
      threeKind2=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity1==cardIdentity2 && cardIdentity1==cardIdentity5){ //if the three cards have the same value
      threeKind3=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity1==cardIdentity3 && cardIdentity1==cardIdentity4){ //if the three cards have the same value
      threeKind4=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity1==cardIdentity3 && cardIdentity1==cardIdentity5){ //if the three cards have the same value
      threeKind5=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity1==cardIdentity4 && cardIdentity1==cardIdentity5){ //if the three cards have the same value
      threeKind6=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity2==cardIdentity3 && cardIdentity2==cardIdentity4){ //if the three cards have the same value
      threeKind7=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity2==cardIdentity3 && cardIdentity2==cardIdentity5){ //if the three cards have the same value
      threeKind8=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity2==cardIdentity4 && cardIdentity2==cardIdentity5){ //if the three cards have the same value
      threeKind9=true;                              //three of a kind evaluates to true
    }                                               //end of if statement
    if(cardIdentity3==cardIdentity4 && cardIdentity3==cardIdentity5){ //if the three cards have the same value
      threeKind10=true;                             //three of a kind evaluates to true
    }                                               //end of if statement
    
    boolean twoPair1=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair2=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair3=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair4=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair5=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair6=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair7=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair8=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair9=false;                         //declares and assigns a potential two pair variable as a boolean
    boolean twoPair10=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair11=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair12=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair13=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair14=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair15=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair16=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair17=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair18=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair19=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair20=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair21=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair22=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair23=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair24=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair25=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair26=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair27=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair28=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair29=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair30=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair31=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair32=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair33=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair34=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair35=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair36=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair37=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair38=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair39=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair40=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair41=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair42=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair43=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair44=false;                        //declares and assigns a potential two pair variable as a boolean
    boolean twoPair45=false;                        //declares and assigns a potential two pair variable as a boolean
    
    
    if(pair1 && pair2){                             //if there are two pair
      twoPair1=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair1 && pair3){                             //if there are two pair
      twoPair2=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair1 && pair4){                             //if there are two pair
      twoPair3=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair1 && pair5){                             //if there are two pair
      twoPair4=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair1 && pair6){                             //if there are two pair
      twoPair5=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair1 && pair7){                             //if there are two pair
      twoPair6=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair1 && pair8){                             //if there are two pair
      twoPair7=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair1 && pair9){                             //if there are two pair
      twoPair8=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair1 && pair10){                            //if there are two pair
      twoPair9=true;                                //two pair evaluates to true
    }                                               //end of if statement
    if(pair2 && pair3){                             //if there are two pair
      twoPair10=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair2 && pair4){                             //if there are two pair
      twoPair11=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair2 && pair5){                             //if there are two pair
      twoPair12=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair2 && pair6){                             //if there are two pair
      twoPair13=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair2 && pair7){                             //if there are two pair
      twoPair14=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair2 && pair8){                             //if there are two pair
      twoPair15=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair2 && pair9){                             //if there are two pair
      twoPair16=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair2 && pair10){                            //if there are two pair
      twoPair17=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair3 && pair4){                             //if there are two pair
      twoPair18=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair3 && pair5){                             //if there are two pair
      twoPair19=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair3 && pair6){                             //if there are two pair
      twoPair20=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair3 && pair7){                             //if there are two pair
      twoPair21=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair3 && pair8){                             //if there are two pair
      twoPair22=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair3 && pair9){                             //if there are two pair
      twoPair23=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair3 && pair10){                            //if there are two pair
      twoPair24=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair4 && pair5){                             //if there are two pair
      twoPair25=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair4 && pair6){                             //if there are two pair
      twoPair26=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair4 && pair7){                             //if there are two pair
      twoPair27=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair4 && pair8){                             //if there are two pair
      twoPair28=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair4 && pair9){                             //if there are two pair
      twoPair29=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair4 && pair10){                            //if there are two pair
      twoPair30=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair5 && pair6){                             //if there are two pair
      twoPair31=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair5 && pair7){                             //if there are two pair
      twoPair32=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair5 && pair8){                             //if there are two pair
      twoPair33=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair5 && pair9){                             //if there are two pair
      twoPair34=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair5 && pair10){                            //if there are two pair
      twoPair35=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair6 && pair7){                             //if there are two pair
      twoPair36=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair6 && pair8){                             //if there are two pair
      twoPair37=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair6 && pair9){                             //if there are two pair
      twoPair38=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair6 && pair10){                            //if there are two pair
      twoPair39=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair7 && pair8){                             //if there are two pair
      twoPair40=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair7 && pair9){                             //if there are two pair
      twoPair41=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair7 && pair10){                            //if there are two pair
      twoPair42=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair8 && pair9){                             //if there are two pair
      twoPair43=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair8 && pair10){                            //if there are two pair
      twoPair44=true;                               //two pair evaluates to true
    }                                               //end of if statement
    if(pair9 && pair10){                            //if there are two pair
      twoPair45=true;                               //two pair evaluates to true
    }                                               //end of if statement
    
    //if statements detecting pairs, two pairs, or three of a kind
    if (threeKind1||threeKind2||threeKind3||threeKind4||threeKind5||threeKind6||threeKind7||threeKind8||threeKind9||threeKind10){
                                                    //if any of the three of a kind statements are true
      System.out.println("You have three of a kind!");  //prints there are three of a kind
    }                                               //end of if statement
    else if (twoPair1||twoPair2||twoPair3||twoPair4||twoPair5||twoPair6||twoPair7||twoPair8||twoPair9||twoPair10||
        twoPair11||twoPair12||twoPair13||twoPair14||twoPair15||twoPair16||twoPair17||twoPair18||twoPair19||twoPair20||
        twoPair21||twoPair22||twoPair23||twoPair24||twoPair25||twoPair26||twoPair27||twoPair28||twoPair29||twoPair30||
        twoPair31||twoPair32||twoPair33||twoPair34||twoPair35||twoPair36||twoPair37||twoPair38||twoPair39||twoPair40||   
        twoPair41||twoPair42||twoPair43||twoPair44||twoPair45){
      System.out.println("You have two pair!");
    }
    else if (pair1||pair2||pair3||pair4||pair5||pair6||pair7||pair8||pair9||pair10){ //if any of the pair statements is true
      System.out.println("You have a pair!");       //prints that there is a pair
    }                                               //end of if statement
    else{                                           //if there is neither a pair, nor two pair, nor three of a kind
      System.out.println("You have a high card hand!"); //prints there is a high card hand
    }                                               //end of else statement
    
    
  }                                                 //end of main method
}                                                   //end of class