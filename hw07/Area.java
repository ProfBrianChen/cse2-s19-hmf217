//Hannah Fabian
//CSE2-S19
//26 March 2019
//
//This program will use methods in order to calculate the area of three different shapes; a rectangle, a triangle and a circle.
//The user will input the dimensions in the form xx.xx.
//
import java.util.Scanner; //imports the existing Scanner class
public class Area{ //states the public class we are in
  
   //determining if the shape input is valid
   public static String validShape(){ //constructs valid method
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Would you like to calculate the area of a rectangle, triangle, or circle? ");
      String myRect="rectangle"; //declares and intitializes myRect
      String myTri="triangle"; //declares and intitializes myTri
      String myCirc="circle"; //declares and intitializes myCirc
      String myShape=myScanner.next(); //accept the user input data as a String
      if (myShape.equals(myRect) || myShape.equals(myTri) || myShape.equals(myCirc)){ //if the input is valid
         return myShape; //return what the shape is
      } //end of if statement
      else{ //if the input is invalid
         while (!myShape.equals(myRect) || !myShape.equals(myTri) || !myShape.equals(myCirc)){ //while the input is invalid
            String junkWord=myShape; //flush out what is in the Scanner
            System.out.println("ERROR: Input is not a valid shape. The valid shapes are: rectangle, triangle, circle.");
            System.out.print("Would you like to calculate the area of a rectangle, triangle, or circle? ");
            myShape=myScanner.next(); //accept the new user input data as a String
            if (myShape.equals(myRect) || myShape.equals(myTri) || myShape.equals(myCirc)){ //if the input is valid
               return myShape; //return what the shape is
            } //end of if statement
         } //end of while loop
      } //end of else statement
      return myShape; //returns myShape for use in main method
   } //end of validShape method
 
   //determining if the numerical input is valid
   public static double validInput(){
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      double myDouble=0; //declares and initializes the input as a double
      boolean isADouble=myScanner.hasNextDouble(); //determines if the input is a double
      if (isADouble==true){ //if the input is a double
         myDouble=myScanner.nextDouble(); //accept the user input data as a double
         while (myDouble<0){ //while the double is negative
            System.out.print("Please enter a number in the form xx.xx: "); //prompts the user to input a double
            isADouble=myScanner.hasNextDouble(); //determines if input is a double
            if (isADouble==true){ //if the input is a double
               myDouble=myScanner.nextDouble(); //accept the user input data as a double
            } //end of if statement
         } //end of while loop
      } //end of if statement
      else{ //if the input is not a double
         while (isADouble==false){ //while the input is not a double
            String junkWord=myScanner.next(); //flush out what is in the Scanner
            System.out.print("Please enter a number in the form xx.xx: "); //prompts the user to input a double
            isADouble=myScanner.hasNextDouble(); //determines if input is a double
            if (isADouble==true){ //if the input is a double
               myDouble=myScanner.nextDouble(); //accept the user input data as a double
            } //end of if statement
         } //end of while loop
      } //end of else statement
      return myDouble; //returns myDouble for use in main method
   } //end of validInput method
   
   //rectangle method
   public static double rect(){ 
      String myShape="rectangle"; //declares and initializes myShape as a String
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Enter the length of the rectangle: ");
      double myLength=validInput(); //calls the method to check if the number is a positive double
      System.out.print("Enter the width of the rectangle: ");
      double myWidth=validInput(); //calls the method to check if the number is a positive double
      double myArea=myLength*myWidth; //calculation for area of a rectangle
      return myArea; //returns myArea for use in the main method
   } //end of rect method
   
   //triangle method
   public static double tri(){
      String myShape="triangle"; //declares and initializes myShape as a String
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Enter the base of the rectangle: ");
      double myBase=validInput(); //calls the method to check if the number is a positive double
      System.out.print("Enter the height of the rectangle: ");
      double myHeight=validInput(); //calls the method to check if the number is a positive double
      double myArea=0.5*myBase*myHeight; //calculation for area of a triangle
      return myArea; //returns myArea for use in the main method
   } //end of tri method
   
   //circle method
   public static double circ(){
      String myShape="circle"; //declares and initializes myShape as a String
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Enter the radius of the circle: ");
      double myRadius=validInput(); //calls the method to check if the number is a positive double
      double myArea=3.1415*myRadius*myRadius; //calculation for area of a circle
      myArea=((int)(myArea*100))/100; //trims off some decimal points
      return myArea; //returns myArea for use in the main method
   } //end of circ method
  
   //main method
   public static void main(String[] args){
      String myShape=validShape(); //declares and initialises myShape from validShape method
      double myArea=0; //delcares and initializes myArea for use below
      String myRect="rectangle"; //declares and intitializes myRect for use below
      String myTri="triangle"; //declares and intitializes myTri for use below
      String myCirc="circle"; //declares and intitializes myCirc for use below
      if (myShape.equals(myRect)){ //if the shape the user entered is "rectangle"
         myArea=rect(); //get the area from the rectangle method
      } //end of if statement
      else if (myShape.equals(myTri)){ //if the shape the user entered is "triangle"
         myArea=tri(); //get the area from the triangle method
      } //end of else if statement
      else if (myShape.equals(myCirc)){ //if the shape the user entered is "circle"
         myArea=circ(); //get the area from the circle method
      } //end of else if statement
      System.out.println("The area of the "+myShape+" equals "+myArea+'.'); //print the shape and area
   } //end of main method
} //end of public class