//Hannah Fabian
//CSE2-S19
//26 March 2019
//
//This program will use methods in order to  process a string by examining all the characters, or just a specified number of //characters in the string, and determine if they are letters. 
//
import java.util.Scanner; //imports the existing Scanner class
public class StringAnalysis{ //public class that we are in
   
//    //method for analyzing all letters
//    public static boolean stringAnalysis(String myWord){
//       Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
//       System.out.print("Enter a String to be analyzed: ");
//       String myString=myScanner.next(); //accept the user input data as a String
//       System.out.print("Enter the number of characters to be analyzed (if all, type 'ALL'): ");
//       String myInput=myScanner.next();
//       myWord="ALL"; //declares and intitializes myWord
//       int myLength=0;
//       if (myInput.equals(myWord)){ //if the input is valid
//          myLength=myString.length();
//       } //end of if statement
      
//       //to determine if it is a letter
//       boolean isLetter=true;
//       for (int counter=0; myLength>counter; counter++){
//          char myChar=myString.charAt(counter);
//          if (myChar<='z' && myChar>='a'){
//             isLetter=true;
//          }
//          else{
//             isLetter=false;
//          }
//       }      
//       return isLetter;
//    } //end of method for analyzing all letters
   
   //method for analyzing only some letters
   public static boolean stringAnalysis(){
      int myInt=0; //declares and initializes the input as an integer
      Scanner myScanner=new Scanner(System.in); //declares and constructs a Scanner that is ready to accept input
      System.out.print("Enter a String to be analyzed: ");
      String myString=myScanner.next(); //accept the user input data as a String
      System.out.print("Do you want to analyze all of the characters? Enter 'yes' or 'no': ");
      String myAnswer=myScanner.next(); //accept the user input data as a String
      if (myAnswer.equals("yes")){ //if the answer is yes
         myInt=myString.length(); //assign the int to be the String length
      } //end of if statement
      else{ //if the answer is no
         //determines if input is valid
         System.out.print("How many of the chaacters do you want to analyze? ");
         myInt=0; //declares and initializes the input as an integer
         boolean isAnInteger=myScanner.hasNextInt(); //determines if the input is an integer
         if (isAnInteger==true){  //if the input is an integer
            myInt=myScanner.nextInt(); //accept the user input data as an integer
            while (myInt<=0){ //while the integer is negative
               System.out.print("Enter an integer: "); //prompts the user to input an integer 
               isAnInteger=myScanner.hasNextInt(); //determines if input is an integer  
               if (isAnInteger==true){ //if the input is an integer
                  myInt=myScanner.nextInt(); //accept the user input data as an integer
               } //end of if statement
            } //end of while loop
         } //end of if statement
         else{ //if the input is not an integer
            while (isAnInteger==false){ //while the input is not an integer
               String junkWord=myScanner.next(); //flush out what is in the Scanner
               System.out.print("Enter an integer: "); //prompts the user to input an integer
               isAnInteger=myScanner.hasNextInt(); //determines if input is an integer
               if (isAnInteger==true){ //if the input is an integer
                  myInt=myScanner.nextInt(); //accept the user input data as an integer
               } //end of if statement
            } //end of while loop
         } //end of else statement
      } //end of else statement
      
      //to determine if it is a letter
      boolean isLetter=true; //declares and initializes isLetter as a boolean
      for (int counter=0; myInt>counter; counter++){ //for all the characters in the String
         char myChar=myString.charAt(counter); //determine what the character is at a certain position
         if (((myString.length())-counter)==0){ //if the integer is longer than the String length
            break; //stop the for loop
         } //end of if statement
         if (myChar<='z' && myChar>='a'){ //if myChar is between a and z
            isLetter=true; //it is a letter
         } //end of if statement
         else{ //if myChar is not between a and z
            isLetter=false; //it is not a letter
         } //end of else statement
      } //end of for loop      
      return isLetter; //returns isLetter as boolean for use below
   } //end of method for analyzing only some letters
   
   //main method
   public static void main(String[] args){
      boolean isLetter=stringAnalysis(); //declare and assign isLetter from stringAnalysis method
      if (isLetter==true){ //if it is a letter
         System.out.println("The part of the String we analyzed consists of all letters!"); //print this
      } //ends of if statement
      else{ //if it is not a letter
         System.out.println("The part of the String we analyzed does NOT consist of all letters."); //print this
      } //ends of else statement
   } //end of main method
} //end of public class