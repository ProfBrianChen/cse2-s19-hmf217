//Hannah Fabian
//8 March 2019
//CSE2-S19
//
//This program will use loops to print a triangle pattern.
//
import java.util.Scanner;                         //imports the existing Scanner class
public class PatternA{                            //states the public class we are in
  public static void main(String[] args){         //main method required for every Java program
    Scanner myScanner=new Scanner(System.in);     //declares and constructs a Scanner that is ready to accept input
    
    //input statements
    System.out.print("Enter an integer between 1 and 10: "); //prompts the user to input an integer between 1 and 10
    int myInt=1;                                  //declares and initializes the input as an integer
    boolean isAnInteger=myScanner.hasNextInt();   //determines if the input is an integer
    
    //determining if the input is valid
    if (isAnInteger==true){                       //if the input is an integer
      myInt=myScanner.nextInt();                  //accept the user input data as an integer
      while (myInt<=0 || myInt>=10){               //while the integer is out of range
        System.out.println("ERROR: Input out of range."); //displays error message
        System.out.print("Enter an integer between 1 and 10: "); //prompts the user to input an integer
        isAnInteger=myScanner.hasNextInt();       //determines if input is an integer
        if (isAnInteger==true){                   //if the input is an integer
          myInt=myScanner.nextInt();              //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (isAnInteger==false){                 //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("ERROR: Input is not an integer."); //displays error message
        System.out.println("Enter an integer: ");   //prompts the user to input an integer
        isAnInteger=myScanner.hasNextInt();       //determines if input is an integer
        if (isAnInteger==true){                   //if the input is an integer
          myInt=myScanner.nextInt();              //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //for loop detrmining what to print
    for (int myRows=1; myRows<=myInt; myRows++){  //while number of rows is less than or equal to integer; increment myRows
      for (int myCols=1; myCols<=myRows; myCols++){ //while number of columns is less than number of rows; increment myCols          
        System.out.print(myCols+" ");                 //print out the number 
      }                                           //end of nested for loop
      System.out.println("");                     //go to the next line
    }                                             //end of for loop           
  }                                               //end of main method
}                                                 //end of public class