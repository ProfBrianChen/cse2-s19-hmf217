//Hannah Fabian
//8 March 2019
//CSE2-S19
//
//This program will use loops to print a triangle pattern.
//
import java.util.Scanner;                         //imports the existing Scanner class
public class PatternC{                            //states the public class we are in
  public static void main(String[] args){         //main method required for every Java program
    Scanner myScanner=new Scanner(System.in);     //declares and constructs a Scanner that is ready to accept input
    
    //input statements
    System.out.print("Enter an integer between 1 and 10: "); //prompts the user to input an integer between 1 and 10
    int myInt=1;                                  //declares and initializes the input as an integer
    boolean isAnInteger=myScanner.hasNextInt();   //determines if the input is an integer
    
    //determining if the input is valid
    if (isAnInteger==true){                       //if the input is an integer
      myInt=myScanner.nextInt();                  //accept the user input data as an integer
      while (myInt<=0 || myInt>=10){               //while the integer is out of range
        System.out.println("ERROR: Input out of range."); //displays error message
        System.out.print("Enter an integer between 1 and 10: "); //prompts the user to input an integer
        isAnInteger=myScanner.hasNextInt();       //determines if input is an integer
        if (isAnInteger==true){                   //if the input is an integer
          myInt=myScanner.nextInt();              //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of if statement
    else{                                         //if the input is not an integer
      while (isAnInteger==false){                 //while the input is not an integer
        String junkWord = myScanner.next();       //flush out what is in the Scanner
        System.out.println("ERROR: Input is not an integer."); //displays error message
        System.out.println("Enter an integer: ");   //prompts the user to input an integer
        isAnInteger=myScanner.hasNextInt();       //determines if input is an integer
        if (isAnInteger==true){                   //if the input is an integer
          myInt=myScanner.nextInt();              //accept the user input data as an integer
        }                                         //end of if statement
      }                                           //end of while loop
    }                                             //end of else statement
    
    //for loop detrmining what to print
    int i=1;                                      //declares and initializes i
    int j=1;                                      //declares and initializes j     
    int k=1;                                      //declares and initializes k
    for (i=1; i<=myInt; ++i, j+=i){               //for loop that determined how many lines are printed
      for (k=1; k<=(myInt-i); ++k){               //for loop that determines where blank spaces will be printed
        System.out.print(" ");                    //prints a blank space
      }                                           //end of for loop
      for (; j>0; --j){                           //for loop that determines where numbers will be printed
        System.out.print(myInt-(myInt-j));        //prints the coressponding number
      }                                           //end of for loop
      System.out.println("");                     //prints a new line
    }                                             //end of first for loop
    System.out.println("");                       //prints a new lines
  }                                               //end of main method
}                                                 //end of public class