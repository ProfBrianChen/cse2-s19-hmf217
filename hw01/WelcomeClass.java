public class WelcomeClass{

  public static void main(String args[]){
    System.out.println("    -----------\n    | WELCOME |\n    -----------\n  ^  ^  ^  ^  ^  ^\n / \\/ \\/ \\/ \\/ \\/ \\\n<-H--M--F--2--1--7->\n \\ /\\ /\\ /\\ /\\ /\\ /\n  v  v  v  v  v  v");    //prints the welcome image of the lab
    System.out.println(" My name is Hannah,\n and I am planning\n on studying electrical\n engineering. I am from\n Easton, PA.");    //prints my autobiographic statement             
  }                   
}