//Hannah Fabian
//Professor Chen
//CSE2-S19
//19 April 2019
//
//This program will use multidimensional arrays.

import java.util.Random; //imports the existing Random class

public class Lab10{ //says the public class we are in
   
   //method for generating a multidimensional array
    public static int[][] increasingMatrix(int myHeight, int myWidth, boolean format){
       if (format==true){ //if the matrix is row major
          int element=0; //initialize the element
          int[][] A=new int[myHeight][myWidth]; //declare and construct a row major array
          for (int i=0; i<myHeight; i++){ //for the height of the array
             for (int j=0; j<myWidth; j++){ //for the width of the array
                A[i][j]=element; //assign the array element
                element++; //increment the element
             } //end of for loop
          } //end of for loop
          return A; //returm A for future use
       } //end of if statement
       else{ //if the format is not row major
          int element=0; //initialize the element
          int[][] A=new int[myWidth][myHeight]; //declare and construct a column major array
          for (int i=0; i<myHeight; i++){ //for the height of the array
             for (int j=0; j<myWidth; j++){ //for the width of the array
                A[j][i]=element; //assign the array element
                element++; //increment the array
             } //end of for loop
          } //end of for loop
          return A; //return A for future use
       } //end of else statement
    } //end of increasingMatrix method
   
   
     //method to print the array
     public static void printMatrix(int[][] A, boolean format){
        if (A==null){ //if the array is empty
           System.out.println("The array is empty!"); //print this
        } //end of if statement
        if (format==true){ //if the array is row major
           for (int i=0; i<A.length; i++){ //for all end elements
              System.out.print("["); //print this
              for (int j=0; j<A[0].length; j++){ //for all elements
                 if (j==A[0].length-1){ //if it is an end element
                    System.out.print(A[i][j]+"]"); //print this
                 } //end of if statement
                 else{ //if it is not an end element
                    System.out.print(A[i][j]+" "); //print the element
                 } //end of else statement
              } //end of for loop
              System.out.println(); //print a new line
           } //end of for loop
        } //end of if statement
        else{ //if the format is not row major
           for (int i=0; i<A[0].length; i++){ //for all end elements
              System.out.print("["); //print this
              for (int j=0; j<A.length; j++){ //for all elements
                 if (j==A.length-1){ //if it is an end element
                    System.out.print(A[j][i]+"]"); //print this
                 } //end of if statement
                 else{ //if it is not an end element
                    System.out.print(A[j][i]+" "); //print the element
                 } //end of else statement
              } //end of if statement
              System.out.println(); //print a new line
           } //end of for loop
        } //end of else statement 
     } //end of printMatrix method
   
   
     //method for changing from column major format to row major format
     public static int[][] translate(int[][] B){
        int[][] translatedB=new int[B[0].length][B.length];
        for (int i=0; i<translatedB.length; i++){
           for (int j=0; j<translatedB[0].length; j++){
              translatedB[i][j]=B[j][i];
           }
        }
        return translatedB;      
     } //end of translate method
   
   
      //method for adding the two arrays
      public static int[][] addMatrix(int[][] A, boolean formata, int[][] B, boolean formatb){
         if (formata==formatb){
            if (A.length==B.length && A[0].length==B[0].length){
               int[][] C=new int[A.length][A[0].length];
               for (int i=0; i<A.length; i++){
                  for (int j=0; j<A[0].length; j++){
                     C[i][j]=A[i][j]+B[i][j];
                  }
               }
               if (formata==false && formatb==false){
                  C=translate(C);
               }
               return C;
            }
            else{
               System.out.println("The arrays cannot be added :(");
               return null;
            }
         }
         else{
            if (A.length==B[0].length && B.length==A[0].length){
               if (formata==false){
                  A=translate(A);
               }
               if (formatb==false){
                  B=translate(B);
               }
               int[][] C=new int[A.length][A[0].length];
               for (int i=0; i<A.length; i++){
                  for (int j=0; j<A[0].length; j++){
                     C[i][j]=A[i][j]+B[i][j];
                  }
               }
               return C;
            }
            else{
               System.out.println("The arrays cannot be added :(");
               return null;
            }
         }
      } //end of addMatrix method
   
   
   //main method
   public static void main(String[] args){
      Random randomGenerator=new Random(); //declares and constructs a Random() object
      int myHeight=randomGenerator.nextInt(5)+1;
      int myWidth=randomGenerator.nextInt(5)+1;
      int myHeight2=randomGenerator.nextInt(5)+1;
      int myWidth2=randomGenerator.nextInt(5)+1;
      int[][] A=increasingMatrix(myHeight, myWidth, true);
      int[][] B=increasingMatrix(myHeight, myWidth, false);
      int[][] C=increasingMatrix(myHeight2, myWidth2, true);
      System.out.println("Generating a matrix with height "+myHeight+" and width "+myWidth+":");
      printMatrix(A, true);
      System.out.println();
      System.out.println("Generating a matrix with height "+myHeight+" and width "+myWidth+":");
      printMatrix(B, false);
      System.out.println();
      System.out.println("Generating a matrix with height "+myHeight2+" and width "+myWidth2+":");
      printMatrix(C, true);
      System.out.println();
      int[][] AB=addMatrix(A, true, B, false);
      System.out.println("Generating matrix A+B:");
      printMatrix(AB, true);   
      System.out.println();
      System.out.println("Generating matrix A+C:");
      int[][] AC=addMatrix(A, true, C, true);
      if (AC!=null){
         printMatrix(AC, true);   
      }
      System.out.println();
   } //end of main method
} //end of public class
